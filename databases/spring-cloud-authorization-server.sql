/*
 Navicat Premium Data Transfer

 Source Server         : Docker-MySQL8.0
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 192.168.99.101:3306
 Source Schema         : spring-cloud-authorization-server

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 02/04/2020 11:39:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for client_details
-- ----------------------------
DROP TABLE IF EXISTS `client_details`;
CREATE TABLE `client_details`(
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户端名称（标题）',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户端备注信息',
  `client_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户端ID',
  `client_secret` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户端密钥',
  `resource_ids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源ID列表',
  `scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '范围列表',
  `authorized_grant_types` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权的授权类型',
  `registered_redirect_uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重定向URI',
  `auto_approve_scopes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自动批准范围',
  `authorities` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限列表',
  `access_token_validity_seconds` int(0) NULL DEFAULT NULL COMMENT '访问令牌有效性-秒',
  `refresh_token_validity_seconds` int(0) NULL DEFAULT NULL COMMENT '刷新令牌有效性-秒',
  `additional_information` json NULL COMMENT '额外的信息',
  `version` int(0) UNSIGNED NULL DEFAULT 0 COMMENT '版本号',
  `gmt_create` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `gmt_modified` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_unique`(`id`, `client_id`) USING BTREE COMMENT '主键和客户端ID唯一',
  INDEX `index_time`(`gmt_create`, `gmt_modified`) USING BTREE COMMENT '时间排序字段',
  INDEX `index_search`(`title`) USING BTREE COMMENT '名称查询索引'
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci COMMENT = '客户端信息'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of client_details
-- ----------------------------
INSERT INTO `client_details`
VALUES ('1', '系统默认客户端', '服务端默认的密码模式登录方式', 'password', '$2a$10$HYzxWtK1ig8IC8Ouavw2vuKuxt35YABb6ihzPrpaxQzvKTMQ3vrh.',
        NULL, NULL, 'password,refresh_token', NULL, 'true', NULL, 3600, 21600, NULL, 0, '2020-03-07 11:42:45.326942',
        '2020-03-28 12:37:34.851298');

-- ----------------------------
-- Table structure for data_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `data_dictionary`;
CREATE TABLE `data_dictionary`
(
    `id`           varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键ID',
    `title`        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '字典类型名称',
    `type`         varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典类型（唯一值）',
    `remarks`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限备注',
    `enabled`      bit(1)                                                        NULL DEFAULT b'0' COMMENT '启用状态',
    `version`      int(0) UNSIGNED                                               NULL DEFAULT 0 COMMENT '修改版本',
    `gmt_create`   timestamp(6)                                                  NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
    `gmt_modified` timestamp(6)                                                  NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `index_type` (`type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '数据字典类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for data_dictionary_value
-- ----------------------------
DROP TABLE IF EXISTS `data_dictionary_value`;
CREATE TABLE `data_dictionary_value`
(
    `id`            varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键ID',
    `dictionary_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关联 数据字典 对象的主键ID',
    `label`         varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '字段名称',
    `value`         varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '字段值',
    `remarks`       varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限备注',
    `enabled`       bit(1)                                                        NULL DEFAULT b'0' COMMENT '启用状态',
    `sorted`        int(0) UNSIGNED                                               NULL DEFAULT 0 COMMENT '数据字典值列表排序值',
    `version`       int(0) UNSIGNED                                               NULL DEFAULT 0 COMMENT '修改版本',
    `gmt_create`    timestamp(6)                                                  NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
    `gmt_modified`  timestamp(6)                                                  NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '数据字典值信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '权限ID',
  `category_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关联权限信息类别ID',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限代码',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限备注',
  `version` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '修改版本',
  `gmt_create` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `gmt_modified` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '权限信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('1', NULL, 'default', '默认权限', '默认权限', 0, '2019-12-08 06:45:51.640853', '2019-12-08 06:45:51.640853');
INSERT INTO `permission` VALUES ('2', NULL, 'test', '测试权限', '测试权限', 0, '2019-12-23 07:40:01.803969', '2019-12-23 07:40:01.803969');

-- ----------------------------
-- Table structure for permission_category
-- ----------------------------
DROP TABLE IF EXISTS `permission_category`;
CREATE TABLE `permission_category`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '权限信息类别ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限信息类别名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限信息类别备注',
  `version` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '修改版本',
  `gmt_create` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `gmt_modified` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '权限信息类别表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色ID',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色代码',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色备注',
  `version` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '修改版本',
  `gmt_create` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `gmt_modified` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'DEFAULT', '默认角色', '默认角色', 0, '2019-12-08 06:36:44.985171', '2019-12-08 06:36:44.985171');
INSERT INTO `role` VALUES ('2', 'ADMIN', '管理员角色', '管理员角色', 0, '2019-12-23 07:35:38.595434', '2019-12-23 07:35:38.595434');

-- ----------------------------
-- Table structure for role_permission_ext
-- ----------------------------
DROP TABLE IF EXISTS `role_permission_ext`;
CREATE TABLE `role_permission_ext`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `related_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_search`(`role_id`, `related_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_permission_ext
-- ----------------------------
INSERT INTO `role_permission_ext` VALUES (1, '1', '1');

-- ----------------------------
-- Table structure for unit
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '部门名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '部门备注',
  `version` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '修改版本',
  `gmt_create` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `gmt_modified` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unit
-- ----------------------------
INSERT INTO `unit` VALUES ('1', '默认部门', '默认部门', 0, '2019-12-08 06:32:33.213174', '2019-12-08 06:32:33.213174');

-- ----------------------------
-- Table structure for unit_permission_ext
-- ----------------------------
DROP TABLE IF EXISTS `unit_permission_ext`;
CREATE TABLE `unit_permission_ext`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unit_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `related_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_search`(`unit_id`, `related_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unit_permission_ext
-- ----------------------------
INSERT INTO `unit_permission_ext` VALUES (1, '1', '1');

-- ----------------------------
-- Table structure for unit_role_ext
-- ----------------------------
DROP TABLE IF EXISTS `unit_role_ext`;
CREATE TABLE `unit_role_ext`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unit_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `related_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_search`(`unit_id`, `related_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unit_role_ext
-- ----------------------------
INSERT INTO `unit_role_ext` VALUES (1, '1', '1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键ID',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户头像',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名（冗余用户帐号信息）',
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号（冗余用户帐号信息）',
  `email` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电子邮件（冗余用户帐号信息）',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `is_account_non_expired` bit(1) NULL DEFAULT NULL COMMENT '帐号是否未过期',
  `is_account_non_locked` bit(1) NULL DEFAULT NULL COMMENT '帐号是否未锁定',
  `is_credentials_non_expired` bit(1) NULL DEFAULT NULL COMMENT '密码是否未过期',
  `is_enabled` bit(1) NULL DEFAULT NULL COMMENT '帐号是否启用',
  `version` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '修改版本',
  `gmt_create` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '记录创建时间',
  `gmt_modified` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '记录修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_unique`(`id`) USING BTREE,
  INDEX `index_search`(`nickname`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ---------------------------- 实际密码为: 123456
INSERT INTO `user` VALUES ('1', '系统管理员', NULL, 'root', '13012345678', 'root@163.com', '$2a$10$IkRnhaal2ucB7ygwEhJmUO2e6RIrj9XGC54KzAe7Lc7EaZYV8IODe', b'1', b'1', b'1', b'1', 0, '2019-12-02 09:41:32.972080', '2020-02-03 16:24:58.696785');

-- ----------------------------
-- Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账户主键ID',
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关联用户ID',
  `identity_type` int(1) NULL DEFAULT NULL COMMENT '登录类型：1用户名、2邮箱、3手机号',
  `identifier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '登录用户识别标识符',
  `credential` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '登录用户授权凭证',
  `is_verified` bit(1) NULL DEFAULT NULL COMMENT '是否验证通过',
  `version` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '修改版本',
  `gmt_create` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `gmt_modified` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_unique`(`id`, `identifier`) USING BTREE,
  INDEX `index_search`(`user_id`, `identity_type`, `identifier`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户登录帐号信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_account
-- ----------------------------
INSERT INTO `user_account` VALUES ('1', '1', 1, 'root', NULL, b'1', 0, '2019-12-04 03:53:59.210647', '2019-12-11 01:28:46.795922');
INSERT INTO `user_account` VALUES ('2', '1', 2, 'root@163.com', NULL, b'1', 0, '2019-12-04 03:54:09.886313', '2019-12-11 01:49:20.356730');
INSERT INTO `user_account` VALUES ('3', '1', 3, '13012345678', NULL, b'1', 0, '2019-12-02 09:41:32.981479', '2019-12-11 01:49:21.900478');

-- ----------------------------
-- Table structure for user_group
-- ----------------------------
DROP TABLE IF EXISTS `user_group`;
CREATE TABLE `user_group`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户组ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户组名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户组备注',
  `version` int(0) UNSIGNED NULL DEFAULT 0 COMMENT '修改版本',
  `gmt_create` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `gmt_modified` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户组信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_group
-- ----------------------------
INSERT INTO `user_group` VALUES ('1', '默认用户组', '默认用户组', 0, '2019-12-08 06:27:16.975304', '2019-12-08 06:27:16.975304');

-- ----------------------------
-- Table structure for user_group_ext
-- ----------------------------
DROP TABLE IF EXISTS `user_group_ext`;
CREATE TABLE `user_group_ext`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `related_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_search`(`user_id`, `related_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_group_ext
-- ----------------------------
INSERT INTO `user_group_ext` VALUES (1, '1', '1');

-- ----------------------------
-- Table structure for user_group_permission_ext
-- ----------------------------
DROP TABLE IF EXISTS `user_group_permission_ext`;
CREATE TABLE `user_group_permission_ext`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `group_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `related_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_search`(`group_id`, `related_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_group_permission_ext
-- ----------------------------
INSERT INTO `user_group_permission_ext` VALUES (1, '1', '1');

-- ----------------------------
-- Table structure for user_group_role_ext
-- ----------------------------
DROP TABLE IF EXISTS `user_group_role_ext`;
CREATE TABLE `user_group_role_ext`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `group_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `related_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_search`(`group_id`, `related_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_group_role_ext
-- ----------------------------
INSERT INTO `user_group_role_ext` VALUES (1, '1', '1');

-- ----------------------------
-- Table structure for user_permission_ext
-- ----------------------------
DROP TABLE IF EXISTS `user_permission_ext`;
CREATE TABLE `user_permission_ext`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `related_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_search`(`user_id`, `related_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_permission_ext
-- ----------------------------
INSERT INTO `user_permission_ext` VALUES (1, '1', '1');
INSERT INTO `user_permission_ext` VALUES (2, '1', '2');

-- ----------------------------
-- Table structure for user_role_ext
-- ----------------------------
DROP TABLE IF EXISTS `user_role_ext`;
CREATE TABLE `user_role_ext`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `related_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_search`(`user_id`, `related_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role_ext
-- ----------------------------
INSERT INTO `user_role_ext` VALUES (1, '1', '1');
INSERT INTO `user_role_ext` VALUES (2, '1', '2');

-- ----------------------------
-- Table structure for user_unit_ext
-- ----------------------------
DROP TABLE IF EXISTS `user_unit_ext`;
CREATE TABLE `user_unit_ext`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `related_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_search`(`user_id`, `related_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_unit_ext
-- ----------------------------
INSERT INTO `user_unit_ext` VALUES (1, '1', '1');

SET FOREIGN_KEY_CHECKS = 1;
