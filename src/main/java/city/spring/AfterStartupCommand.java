package city.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 启动后执行命令
 *
 * @author HouKunLin
 * @date 2019/12/5 0005 9:47
 */
@Component
public class AfterStartupCommand implements CommandLineRunner {
    private final static Logger logger = LoggerFactory.getLogger(AfterStartupCommand.class);
    private final boolean isDebug = logger.isDebugEnabled();
    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void run(String... args) throws Exception {
    }
}
