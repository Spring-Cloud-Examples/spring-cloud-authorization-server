package city.spring;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Repository;

/**
 * 授权服务应用
 *
 * @author HouKunLin
 * @date 2019/12/1 0001 22:25
 */
@EnableCaching
@SpringBootApplication
@ConfigurationPropertiesScan
@ServletComponentScan(basePackageClasses = {SpringCloudAuthorizationServerApplication.class})
@MapperScan(annotationClass = Repository.class, basePackageClasses = SpringCloudAuthorizationServerApplication.class)
public class SpringCloudAuthorizationServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudAuthorizationServerApplication.class, args);
    }

}
