package city.spring.configure;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * 日期转换配置。
 * 在 SpringBoot 中，日期类型数据转换需要配置两个位置，一个是 SpringMVC 的日期类型参数转换，另一个是 Jackson 的 json 对象的日期类型参数转换。
 * 因此，需要配置 SpringMVC 的 Converter 对象，以及在 Jackson 的 Module 中配置 JsonSerializer 和 JsonDeserializer
 *
 * @author HouKunLin
 * @date 2020/3/17 0017 13:56
 */
@Configuration
public class CustomDateConverterConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(CustomDateConverterConfiguration.class);
    /**
     * Jackson 的 ObjectMapper 对象
     */
    private final ObjectMapper objectMapper;
    /**
     * Java 8 日期时间格式化对象列表
     */
    private static List<DateTimeFormatter> dateTimeFormatters = new ArrayList<>();
    /**
     * 普通日期时间格式化对象列表
     */
    private static List<DateFormat> dateFormats = new ArrayList<>();

    static {
        // 使日期格式能够兼容我们常使用的格式
        String[] formats = new String[]{
                "yyyy-MM-dd HH:mm:ss.SSS",
                "yyyy-MM-dd HH:mm:ss",
                "yyyy-MM-dd",
                "HH:mm:ss",
        };
        for (String format : formats) {
            dateFormats.add(new SimpleDateFormat(format));
            dateTimeFormatters.add(DateTimeFormatter.ofPattern(format));
        }
    }

    public CustomDateConverterConfiguration(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    public void postConstruct() {
        SimpleModule module = new SimpleModule();
        // 增强的 Date 反序列化，使其支持更多的格式信息
        module.addDeserializer(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
                JsonNode node = jsonParser.getCodec().readTree(jsonParser);
                return convertDate(node.textValue());
            }
        });
        // Joda DateTime 序列化，把它转换为 Date 对象，然后由默认的 Date 序列化处理器来处理
        module.addSerializer(DateTime.class, new JsonSerializer<DateTime>() {
            @Override
            public void serialize(DateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
                gen.writeObject(value.toDate());
            }
        });
        // Joda DateTime 反序列化
        module.addDeserializer(DateTime.class, new JsonDeserializer<DateTime>() {
            @Override
            public DateTime deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException {
                JsonNode node = jsonParser.getCodec().readTree(jsonParser);
                return convertDateTime(node.textValue());
            }
        });
        objectMapper.registerModule(module);
        logger.debug("配置自定义日期时间格式转换：{}", this);
    }

    /**
     * 使用自定义的日期格式来解析为 Date。
     * 仅支持 {@link #dateFormats} 中定义的格式。
     *
     * @param source 日期字符串
     * @return Date
     */
    @Nullable
    public static Date parseDate(@NonNull String source) {
        source = source.trim();
        if (source.isEmpty()) {
            return null;
        }
        for (DateFormat dateFormat : dateFormats) {
            try {
                return dateFormat.parse(source);
            } catch (ParseException ignored) {
            }
        }
        throw new RuntimeException("无法解析" + source + "为" + Date.class.getName() + "类型");
    }

    /**
     * 使用自定义的日期格式来解析为本地时间，会优先使用默认的 ISO 标准格式来解析，如果解析失败才使用自定义的格式来解析。
     * 可支持 <code>yyyy-MM-dd'T'HH:mm:ss</code> 和 {@link #dateFormats} 中定义的格式。
     *
     * @param source         日期字符串
     * @param defaultParse   默认的解析方法，使用 ISO 标准格式解析
     * @param formatterParse 需要传入 DateTimeFormatter 的解析方法，使用自定义的格式来解析
     * @param <T>            本地实际类型：LocalDateTime / LocalDate / LocalTime
     * @return LocalDateTime / LocalDate / LocalTime
     */
    @Nullable
    public static <T> T parseDate(@NonNull String source, Function<CharSequence, T> defaultParse, BiFunction<CharSequence, DateTimeFormatter, T> formatterParse) {
        source = source.trim();
        if (source.isEmpty()) {
            return null;
        }
        try {
            return defaultParse.apply(source);
        } catch (Exception e1) {
            for (DateTimeFormatter formatter : dateTimeFormatters) {
                try {
                    return formatterParse.apply(source, formatter);
                } catch (Exception ignored) {
                }
            }
        }
        throw new RuntimeException("无法解析'" + source + "'为本地时间日期类型");
    }

    /**
     * 转换为 Date 对象。默认使用 Joda 解析日期字符串，如果解析失败则使用定义好的日期格式来解析为 Date。
     * 可支持 <code>yyyy-MM-dd'T'HH:mm:ss.SSS</code> <code>yyyy-MM-dd'T'HH:mm:ss.SSS'Z'</code> 和 {@link #dateFormats} 中定义的格式。
     *
     * @param source 日期字符串
     * @return Date
     */
    @Nullable
    public static Date convertDate(@NonNull String source) {
        source = source.trim();
        if (source.isEmpty()) {
            return null;
        }
        try {
            return new DateTime(source).toDate();
        } catch (Exception e) {
            return parseDate(source);
        }
    }

    /**
     * 转换为 DateTime 对象。默认使用 Joda 解析日期字符串，如果解析失败则使用定义好的日期格式来解析为 Date 再转换为 DateTime。
     * 可支持 <code>yyyy-MM-dd'T'HH:mm:ss.SSS</code> <code>yyyy-MM-dd'T'HH:mm:ss.SSS'Z'</code> 和 {@link #dateFormats} 中定义的格式。
     *
     * @param source 日期字符串
     * @return DateTime
     */
    @Nullable
    public static DateTime convertDateTime(@NonNull String source) {
        source = source.trim();
        if (source.isEmpty()) {
            return null;
        }
        try {
            return new DateTime(source);
        } catch (Exception e) {
            return new DateTime(parseDate(source));
        }
    }

    /**
     * 普通 Date 转换
     *
     * @see CustomDateConverterConfiguration#convertDate(String)
     */
    @Component
    public static class DateConverter implements Converter<String, Date> {
        @Override
        public Date convert(@NonNull String source) {
            return convertDate(source);
        }
    }

    /**
     * Joda 对象转换
     *
     * @see CustomDateConverterConfiguration#convertDateTime(String)
     */
    @Component
    public static class DateTimeConverter implements Converter<String, DateTime> {
        @Override
        public DateTime convert(@NonNull String source) {
            return convertDateTime(source);
        }
    }

    /**
     * 本地日期时间转换
     *
     * @see CustomDateConverterConfiguration#parseDate(String, Function, BiFunction)
     */
    @Component
    public static class LocalDateTimeConverter implements Converter<String, LocalDateTime> {
        @Override
        public LocalDateTime convert(@NonNull String source) {
            return parseDate(source, LocalDateTime::parse, LocalDateTime::parse);
        }
    }

    /**
     * 本地日期转换
     *
     * @see CustomDateConverterConfiguration#parseDate(String, Function, BiFunction)
     */
    @Component
    public static class LocalDateConverter implements Converter<String, LocalDate> {
        @Override
        public LocalDate convert(@NonNull String source) {
            return parseDate(source, LocalDate::parse, LocalDate::parse);
        }
    }

    /**
     * 本地时间转换
     *
     * @see CustomDateConverterConfiguration#parseDate(String, Function, BiFunction)
     */
    @Component
    public static class LocalTimeConverter implements Converter<String, LocalTime> {
        @Override
        public LocalTime convert(@NonNull String source) {
            return parseDate(source, LocalTime::parse, LocalTime::parse);
        }
    }
}
