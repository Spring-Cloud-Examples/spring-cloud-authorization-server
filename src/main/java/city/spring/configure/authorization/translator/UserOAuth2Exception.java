package city.spring.configure.authorization.translator;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

import javax.servlet.http.HttpServletRequest;

/**
 * 定义在登录时的错误信息，然后进行捕获，并按照自定义的格式进行输出（UserOAuth2ExceptionSerializer.class）
 *
 * @author HouKunLin
 */
@JsonSerialize(using = UserOAuth2ExceptionSerializer.class)
public class UserOAuth2Exception extends OAuth2Exception {
    private HttpStatus status;
    private HttpServletRequest request;


    public UserOAuth2Exception(HttpStatus httpStatus, OAuth2Exception t, HttpServletRequest request) {
        super(t.getMessage(), t);
        status = httpStatus;
        this.request = request;
    }

    @Override
    public int getHttpErrorCode() {
        return status.value();
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpStatus getStatus() {
        return status;
    }
}