package city.spring.configure.security;

import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/**
 * 自定义授权请求自定义程序
 *
 * @author HouKunLin
 * @date 2019/12/1 0001 22:18
 */
public class CustomAuthorizeRequestsCustomizer implements Customizer<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry> {
    @Override
    public void customize(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry) {
        expressionInterceptUrlRegistry.requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll();
        expressionInterceptUrlRegistry.antMatchers("/test/**", "/.well-known/jwks.json").permitAll();
        expressionInterceptUrlRegistry.anyRequest().authenticated();
    }
}
