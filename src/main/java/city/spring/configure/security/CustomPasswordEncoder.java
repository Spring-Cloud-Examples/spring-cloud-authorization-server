package city.spring.configure.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 自定义密码编码器
 *
 * @author HouKunLin
 * @date 2019/12/3 0003 17:41
 */
@Component
public class CustomPasswordEncoder implements PasswordEncoder {
    private final static Logger logger = LoggerFactory.getLogger(CustomPasswordEncoder.class);
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public String encode(CharSequence rawPassword) {
        return passwordEncoder.encode(rawPassword);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    @Override
    public boolean upgradeEncoding(String encodedPassword) {
        return passwordEncoder.upgradeEncoding(encodedPassword);
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("自定义密码编码器: {}", this);
    }
}
