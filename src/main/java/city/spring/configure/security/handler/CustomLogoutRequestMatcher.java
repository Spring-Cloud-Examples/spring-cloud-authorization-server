package city.spring.configure.security.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

/**
 * 自定义注销请求匹配器
 *
 * @author HouKunLin
 * @date 2019/12/1 0001 22:17
 */
@Component
public class CustomLogoutRequestMatcher implements RequestMatcher {
    private final static Logger logger = LoggerFactory.getLogger(CustomLogoutRequestMatcher.class);
    private final RequestMatcher matcher = new AntPathRequestMatcher("/logout");

    @Override
    public boolean matches(HttpServletRequest request) {
        String actualHeaderValue = request.getParameter("ACTION");
        return "LOGOUT".equals(actualHeaderValue) || matcher.matches(request);
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("自定义注销请求匹配器: {}", this);
    }
}
