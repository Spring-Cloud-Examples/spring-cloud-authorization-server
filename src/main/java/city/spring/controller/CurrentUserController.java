package city.spring.controller;

import city.spring.domain.dto.UserDTO;
import city.spring.domain.entity.UserEntity;
import city.spring.domain.mapper.UserMapper;
import city.spring.service.UserService;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 当前登录用户信息
 *
 * @author HouKunLin
 * @date 2020/1/5 0005 1:25
 */
@RestController
public class CurrentUserController {
    private static final Logger logger = LoggerFactory.getLogger(CurrentUserController.class);
    private final UserService userService;
    private final UserMapper userMapper;

    public CurrentUserController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    /**
     * 获取当前登录用户信息
     *
     * @param user 当前登录用户
     * @return 用户信息
     */
    @RequestMapping("/currentUser")
    public Object currentUser(@NonNull Principal user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        boolean loadAuthority = false;
        if (user instanceof OAuth2Authentication) {
            // 使用默认的登录时的权限信息
            authorities.addAll(((OAuth2Authentication) user).getAuthorities());
        } else {
            loadAuthority = true;
            logger.warn("Current Principal is '{}' not 'OAuth2Authentication'", user.getClass());
        }
        UserEntity userEntity = userService.getUserByIdOrAccount(user.getName(), loadAuthority, UserService.UserInfoEnum.ALL);
        UserDTO userDTO = userMapper.toDto(userEntity);
        if (loadAuthority) {
            // 没有默认的权限信息，使用系统中最新的权限信息
            authorities.addAll(userEntity.getAllAuthorities()
                    .stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList()));
        }
        userDTO.setAuthorities(authorities);
        return userDTO;
    }

    /**
     * 获取 OAuth2 登录用户信息
     * <p>传入参数类型 Principal 实际上为 OAuth2Authentication 对象</p>
     * <p>传入参数类型 Authentication 实际上为 OAuth2Authentication 对象</p>
     *
     * @param user 用户信息
     * @return 信息
     */
    @RequestMapping("/principal")
    public Principal principal(Principal user) {
        return user;
    }
}
