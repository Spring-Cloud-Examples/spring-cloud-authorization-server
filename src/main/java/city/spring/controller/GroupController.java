package city.spring.controller;

import city.spring.domain.entity.GroupEntity;
import city.spring.service.GroupService;
import city.spring.utils.MyBatisUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户组信息 控制器
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 17:40
 */
@RestController
@RequestMapping("/group")
public class GroupController extends ApiController {
    private final GroupService groupService;
    /**
     * 实体类可排序字段
     */
    private final List<SFunction<GroupEntity, ?>> entityOrderFields;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
        entityOrderFields = new ArrayList<>();
        entityOrderFields.add(GroupEntity::getId);
        entityOrderFields.add(GroupEntity::getTitle);
        entityOrderFields.add(GroupEntity::getRemarks);
        entityOrderFields.add(GroupEntity::getGmtCreate);
        entityOrderFields.add(GroupEntity::getGmtModified);
    }

    /**
     * 获取全部的 <strong>用户组信息</strong> 列表
     */
    @GetMapping("all")
    public Object listAll(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, GroupEntity entity) {
        LambdaQueryChainWrapper<GroupEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        return success(lambdaQuery.list());
    }

    /**
     * 分页获取 <strong>用户组信息</strong> 列表
     *
     * @param pageable 分页参数信息
     */
    @GetMapping
    public Object list(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, GroupEntity entity) {
        LambdaQueryChainWrapper<GroupEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        Page<GroupEntity> page = lambdaQuery.page(MyBatisUtils.toPage(pageable, false));
        return success(page);
    }

    /**
     * 获取一个 <strong>用户组信息</strong>
     *
     * @param id 主键ID
     */
    @GetMapping("{id}")
    public Object info(@PathVariable String id) {
        GroupEntity entity = groupService.getGroupInfo(id, true, true);
        return success(entity);
    }

    /**
     * 添加一个 <strong>用户组信息</strong>
     *
     * @param entity 新增的信息
     */
    @PostMapping
    public Object add(@RequestBody GroupEntity entity) {
        groupService.saveGroup(entity);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, entity.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    /**
     * 修改一个 <strong>用户组信息</strong>
     *
     * @param id     主键ID
     * @param entity 修改后的信息
     */
    @PutMapping("{id}")
    public Object update(@PathVariable String id, @RequestBody GroupEntity entity) {
        entity.setId(id);
        groupService.updateGroup(entity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 删除一个 <strong>用户组信息</strong>
     *
     * @param id 主键ID
     */
    @DeleteMapping("{id}")
    public Object delete(@PathVariable String id) {
        groupService.deleteGroup(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * 删除多个 <strong>用户组信息</strong>
     *
     * @param ids 主键ID列表
     */
    @DeleteMapping
    public Object deleteIds(@RequestBody List<String> ids) {
        groupService.deleteGroup(ids);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * 构建查询条件内容
     *
     * @param entity 实体对象
     * @return lambda query chain wrapper
     */
    private LambdaQueryChainWrapper<GroupEntity> buildLambdaQuery(GroupEntity entity) {
        LambdaQueryChainWrapper<GroupEntity> lambdaQuery = groupService.lambdaQuery();
        lambdaQuery.eq(StringUtils.isNotBlank(entity.getId()), GroupEntity::getId, entity.getId());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getTitle()), GroupEntity::getTitle, entity.getTitle());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getRemarks()), GroupEntity::getRemarks, entity.getRemarks());
        return lambdaQuery;
    }
}
