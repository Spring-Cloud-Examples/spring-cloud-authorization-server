package city.spring.controller;

import city.spring.domain.entity.PermissionCategoryEntity;
import city.spring.service.PermissionCategoryService;
import city.spring.utils.MyBatisUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限信息类别控制器
 *
 * @author HouKunLin
 * @date 2020/3/26 0026 9:34
 */
@RestController
@RequestMapping("/permission/category")
public class PermissionCategoryController extends ApiController {
    /**
     * <strong>权限信息类别</strong> Service
     */
    private final PermissionCategoryService permissionCategoryService;
    /**
     * 实体类可排序字段
     */
    private final List<SFunction<PermissionCategoryEntity, ?>> entityOrderFields;

    public PermissionCategoryController(PermissionCategoryService permissionCategoryService) {
        this.permissionCategoryService = permissionCategoryService;
        entityOrderFields = new ArrayList<>();
        entityOrderFields.add(PermissionCategoryEntity::getId);
        entityOrderFields.add(PermissionCategoryEntity::getTitle);
        entityOrderFields.add(PermissionCategoryEntity::getRemarks);
        entityOrderFields.add(PermissionCategoryEntity::getGmtCreate);
        entityOrderFields.add(PermissionCategoryEntity::getGmtModified);
    }

    /**
     * 获取全部的 <strong>权限信息类别</strong> 列表
     */
    @GetMapping("all")
    public Object istAll(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, PermissionCategoryEntity entity) {
        LambdaQueryChainWrapper<PermissionCategoryEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        return success(lambdaQuery.list());
    }

    /**
     * 分页获取 <strong>权限信息类别</strong> 列表
     *
     * @param pageable 分页参数信息
     */
    @GetMapping
    public Object list(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, PermissionCategoryEntity entity) {
        LambdaQueryChainWrapper<PermissionCategoryEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        Page<PermissionCategoryEntity> page = lambdaQuery.page(MyBatisUtils.toPage(pageable, false));
        return success(page);
    }

    /**
     * 获取一个 <strong>权限信息类别</strong>
     *
     * @param id 主键ID
     */
    @GetMapping("{id}")
    public Object info(@PathVariable String id) {
        return success(permissionCategoryService.getById(id));
    }

    /**
     * 添加一个 <strong>权限信息类别</strong>
     *
     * @param entity 新增的信息
     */
    @PostMapping
    public Object add(@RequestBody PermissionCategoryEntity entity) {
        permissionCategoryService.save(entity);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, entity.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    /**
     * 修改一个 <strong>权限信息类别</strong>
     *
     * @param id     主键ID
     * @param entity 修改后的信息
     */
    @PutMapping("{id}")
    public Object update(@PathVariable String id, @RequestBody PermissionCategoryEntity entity) {
        permissionCategoryService.lambdaUpdate()
                .set(PermissionCategoryEntity::getId, entity.getId())
                .eq(PermissionCategoryEntity::getId, id)
                .update();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 删除一个 <strong>权限信息类别</strong>
     *
     * @param id 主键ID
     */
    @DeleteMapping("{id}")
    public Object delete(@PathVariable String id) {
        permissionCategoryService.removeById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    /**
     * 构建查询条件内容
     *
     * @param entity 实体对象
     * @return lambda query chain wrapper
     */
    private LambdaQueryChainWrapper<PermissionCategoryEntity> buildLambdaQuery(PermissionCategoryEntity entity) {
        LambdaQueryChainWrapper<PermissionCategoryEntity> lambdaQuery = permissionCategoryService.lambdaQuery();
        lambdaQuery.eq(StringUtils.isNotBlank(entity.getId()), PermissionCategoryEntity::getId, entity.getId());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getTitle()), PermissionCategoryEntity::getTitle, entity.getTitle());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getRemarks()), PermissionCategoryEntity::getRemarks, entity.getRemarks());
        return lambdaQuery;
    }
}
