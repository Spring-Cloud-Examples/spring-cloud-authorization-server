package city.spring.controller;

import city.spring.domain.entity.PermissionEntity;
import city.spring.service.PermissionService;
import city.spring.utils.MyBatisUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限信息控制器
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 17:40
 */
@RestController
@RequestMapping("/permission")
public class PermissionController extends ApiController {
    private final PermissionService permissionService;
    /**
     * 实体类可排序字段
     */
    private final List<SFunction<PermissionEntity, ?>> entityOrderFields;

    public PermissionController(PermissionService permissionService) {
        this.permissionService = permissionService;
        entityOrderFields = new ArrayList<>();
        entityOrderFields.add(PermissionEntity::getId);
        entityOrderFields.add(PermissionEntity::getTitle);
        entityOrderFields.add(PermissionEntity::getCode);
        entityOrderFields.add(PermissionEntity::getRemarks);
        entityOrderFields.add(PermissionEntity::getGmtCreate);
        entityOrderFields.add(PermissionEntity::getGmtModified);
    }

    /**
     * 获取全部的权限列表信息
     */
    @GetMapping("all")
    public Object listAll(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, PermissionEntity entity) {
        LambdaQueryChainWrapper<PermissionEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        return success(lambdaQuery.list());
    }

    /**
     * 分页获取权限列表信息
     *
     * @param pageable 分页参数信息
     */
    @GetMapping
    public Object list(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, PermissionEntity entity) {
        LambdaQueryChainWrapper<PermissionEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        Page<PermissionEntity> page = lambdaQuery.page(MyBatisUtils.toPage(pageable, false));
        return success(page);
    }

    /**
     * 获取一个权限信息
     *
     * @param id 主键ID
     */
    @GetMapping("{id}")
    public Object info(@PathVariable String id) {
        return success(permissionService.getById(id));
    }

    /**
     * 添加一个权限信息
     *
     * @param entity 修改后的信息
     */
    @PostMapping
    public Object add(@RequestBody PermissionEntity entity) {
        permissionService.savePermission(entity);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, entity.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    /**
     * 修改一个权限信息
     *
     * @param id     主键ID
     * @param entity 修改后的信息
     */
    @PutMapping("{id}")
    public Object update(@PathVariable String id, @RequestBody PermissionEntity entity) {
        entity.setId(id);
        permissionService.updatePermission(entity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 删除一个权限信息
     *
     * @param id 主键ID
     */
    @DeleteMapping("{id}")
    public Object delete(@PathVariable String id) {
        permissionService.deletePermission(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * 删除多个权限信息
     *
     * @param ids 主键ID列表
     */
    @DeleteMapping
    public Object deleteIds(@RequestBody List<String> ids) {
        permissionService.deletePermission(ids);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * 构建查询条件内容
     *
     * @param entity 实体对象
     * @return lambda query chain wrapper
     */
    private LambdaQueryChainWrapper<PermissionEntity> buildLambdaQuery(PermissionEntity entity) {
        LambdaQueryChainWrapper<PermissionEntity> lambdaQuery = permissionService.lambdaQuery();
        lambdaQuery.eq(StringUtils.isNotBlank(entity.getId()), PermissionEntity::getId, entity.getId());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getTitle()), PermissionEntity::getTitle, entity.getTitle());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getCode()), PermissionEntity::getCode, entity.getCode());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getRemarks()), PermissionEntity::getRemarks, entity.getRemarks());
        return lambdaQuery;
    }


    /**
     * 构建查询条件内容
     *
     * @param entity 实体对象
     * @return query wrapper
     */
    private QueryWrapper<PermissionEntity> buildQueryWrapper(PermissionEntity entity) {
        QueryWrapper<PermissionEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(entity.getId()), "id", entity.getId());
        wrapper.like(StringUtils.isNotBlank(entity.getTitle()), "title", entity.getTitle());
        wrapper.like(StringUtils.isNotBlank(entity.getCode()), "code", entity.getCode());
        wrapper.like(StringUtils.isNotBlank(entity.getRemarks()), "remarks", entity.getRemarks());
        return wrapper;
    }
}
