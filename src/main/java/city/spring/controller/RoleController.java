package city.spring.controller;

import city.spring.domain.entity.PermissionEntity;
import city.spring.domain.entity.RoleEntity;
import city.spring.modules.ext.EntityExtUtils;
import city.spring.modules.ext.entity.GroupRoleExt;
import city.spring.modules.ext.entity.RolePermissionExt;
import city.spring.modules.ext.entity.UnitRoleExt;
import city.spring.modules.ext.entity.UserRoleExt;
import city.spring.modules.ext.service.GroupRoleExtService;
import city.spring.modules.ext.service.RolePermissionExtService;
import city.spring.modules.ext.service.UnitRoleExtService;
import city.spring.modules.ext.service.UserRoleExtService;
import city.spring.service.RoleService;
import city.spring.utils.MyBatisUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色信息控制器
 *
 * @author HouKunLin
 * @date 2019/12/20 0020 15:44
 */
@RestController
@RequestMapping("/role")
public class RoleController extends ApiController {
    private final RoleService roleService;
    /**
     * 实体类可排序字段
     */
    private final List<SFunction<RoleEntity, ?>> entityOrderFields;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
        entityOrderFields = new ArrayList<>();
        entityOrderFields.add(RoleEntity::getId);
        entityOrderFields.add(RoleEntity::getTitle);
        entityOrderFields.add(RoleEntity::getCode);
        entityOrderFields.add(RoleEntity::getRemarks);
        entityOrderFields.add(RoleEntity::getGmtCreate);
        entityOrderFields.add(RoleEntity::getGmtModified);
    }

    @GetMapping("all")
    public Object listAll(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, RoleEntity entity) {
        LambdaQueryChainWrapper<RoleEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        return success(lambdaQuery.list());
    }

    /**
     * 获取角色列表信息
     *
     * @param pageable 分页参数信息
     * @return 返回结果
     */
    @GetMapping
    public R<Page<RoleEntity>> list(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, RoleEntity entity) {
        LambdaQueryChainWrapper<RoleEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        Page<RoleEntity> page = lambdaQuery.page(MyBatisUtils.toPage(pageable, false));
        return success(page);
    }

    @GetMapping("{id}")
    public Object info(@PathVariable String id) {
        RoleEntity entity = roleService.getById(id);
        roleService.loadPermissions(entity);
        return success(entity);
    }

    /**
     * 新增角色信息
     *
     * @param entity 接收的数据信息
     */
    @PostMapping
    public Object add(@RequestBody RoleEntity entity) {
        roleService.saveRole(entity);

        HttpHeaders headers = new org.springframework.http.HttpHeaders();
        headers.add(HttpHeaders.LOCATION, entity.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    /**
     * 修改角色信息
     *
     * @param id     角色ID
     * @param entity 角色信息
     */
    @PutMapping("{id}")
    public Object update(@PathVariable String id, @RequestBody RoleEntity entity) {
        entity.setId(id);
        roleService.updateRole(entity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 删除一个角色信息
     *
     * @param id 角色ID
     */
    @DeleteMapping("{id}")
    public Object delete(@PathVariable String id) {
        roleService.deleteRole(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * 删除多个 <strong>角色信息</strong>
     *
     * @param ids 主键ID列表
     */
    @DeleteMapping
    public Object deleteIds(@RequestBody List<String> ids) {
        roleService.deleteRole(ids);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * 设置角色的权限列表
     *
     * @param entity 接收的数据信息
     */
    @PutMapping("set-permission")
    public Object setPermission(@RequestBody RoleEntity entity) {
        roleService.setRolePermission(entity);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 构建查询条件内容
     *
     * @param entity 实体对象
     * @return lambda query chain wrapper
     */
    private LambdaQueryChainWrapper<RoleEntity> buildLambdaQuery(RoleEntity entity) {
        LambdaQueryChainWrapper<RoleEntity> lambdaQuery = roleService.lambdaQuery();
        lambdaQuery.eq(StringUtils.isNotBlank(entity.getId()), RoleEntity::getId, entity.getId());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getTitle()), RoleEntity::getTitle, entity.getTitle());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getCode()), RoleEntity::getCode, entity.getCode());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getRemarks()), RoleEntity::getRemarks, entity.getRemarks());
        return lambdaQuery;
    }
}
