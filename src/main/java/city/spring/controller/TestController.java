package city.spring.controller;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.*;

/**
 * 测试接口信息
 *
 * @author HouKunLin
 * @date 2019/12/3 0003 21:53
 */
@Slf4j
@Data
@RestController
@RequestMapping
public class TestController {
    private final ApplicationContext applicationContext;
    private final HttpServletRequest request;

    public TestController(ApplicationContext applicationContext, HttpServletRequest request) {
        this.applicationContext = applicationContext;
        this.request = request;
    }

    /**
     * 测试普通的请求
     *
     * @param params 参数信息
     * @param user   当前登录用户信息
     * @return 结果
     */
    @RequestMapping(value = {"/", "/index", "/index.html", "/test/test"})
    public Object indexTest(@RequestParam Map<String, Object> params, Principal user) {
        return timeZone(user, params, null);
    }

    /**
     * 测试有请求体的请求
     *
     * @param params 参数信息
     * @param body   请求体信息
     * @param user   当前登录用户信息
     * @return 结果
     */
    @RequestMapping(value = {"/", "/index", "/index.html", "/test/test"}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public Object indexTest(@RequestParam Map<String, Object> params, @RequestBody Map<String, Object> body, Principal user) {
        return timeZone(user, params, body);
    }

    /**
     * 构建返回信息
     *
     * @param user   当前登录用户信息
     * @param params 参数信息
     * @param body   请求体信息
     * @return 返回结果信息
     */
    private Map<String, Object> timeZone(Principal user, Map<String, Object> params, Map<String, Object> body) {
        TimeZone timeZone = TimeZone.getDefault();
        HashMap<String, Object> map = new LinkedHashMap<>(16);
        map.put("ID", timeZone.getID());
        map.put("displayName", timeZone.getDisplayName());
        map.put("zoneId", timeZone.toZoneId());
        map.put("DSTSavings", timeZone.getDSTSavings());
        map.put("useDaylightTime", timeZone.useDaylightTime());
        map.put("observesDaylightTime", timeZone.observesDaylightTime());
        map.put("applicationId", applicationContext.getId());
        map.put("applicationName", applicationContext.getApplicationName());
        map.put("applicationDisplayName", applicationContext.getDisplayName());
        map.put("applicationBeanDefinitionCount", applicationContext.getBeanDefinitionCount());
        map.put("timestamp", new Date());
        map.put("method", request.getMethod());
        map.put("requestQueryString", request.getQueryString());
        map.put("params", params);
        map.put("body", body);
        map.put("user", user);
        return map;
    }
}
