package city.spring.controller;

import city.spring.domain.entity.UnitEntity;
import city.spring.service.UnitService;
import city.spring.utils.MyBatisUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 部门信息 控制器
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 17:40
 */
@RestController
@RequestMapping("/unit")
public class UnitController extends ApiController {
    private final UnitService unitService;
    /**
     * 实体类可排序字段
     */
    private final List<SFunction<UnitEntity, ?>> entityOrderFields;

    public UnitController(UnitService unitService) {
        this.unitService = unitService;
        entityOrderFields = new ArrayList<>();
        entityOrderFields.add(UnitEntity::getId);
        entityOrderFields.add(UnitEntity::getTitle);
        entityOrderFields.add(UnitEntity::getRemarks);
        entityOrderFields.add(UnitEntity::getGmtCreate);
        entityOrderFields.add(UnitEntity::getGmtModified);
    }

    /**
     * 获取全部的 <strong>部门信息</strong> 列表
     */
    @GetMapping("all")
    public Object listAll(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, UnitEntity entity) {
        LambdaQueryChainWrapper<UnitEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        return success(lambdaQuery.list());
    }

    /**
     * 分页获取 <strong>部门信息</strong> 列表
     *
     * @param pageable 分页参数信息
     */
    @GetMapping
    public Object list(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, UnitEntity entity) {
        LambdaQueryChainWrapper<UnitEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        Page<UnitEntity> page = lambdaQuery.page(MyBatisUtils.toPage(pageable, false));
        return success(page);
    }

    /**
     * 获取一个 <strong>部门信息</strong>
     *
     * @param id 主键ID
     */
    @GetMapping("{id}")
    public Object info(@PathVariable String id) {
        UnitEntity entity = unitService.getUnitInfo(id, true, true);
        return success(entity);
    }

    /**
     * 添加一个 <strong>部门信息</strong>
     *
     * @param entity 新增的信息
     */
    @PostMapping
    public Object add(@RequestBody UnitEntity entity) {
        unitService.saveUnit(entity);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, entity.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    /**
     * 修改一个 <strong>部门信息</strong>
     *
     * @param id     主键ID
     * @param entity 修改后的信息
     */
    @PutMapping("{id}")
    public Object update(@PathVariable String id, @RequestBody UnitEntity entity) {
        entity.setId(id);
        unitService.updateUnit(entity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 删除一个 <strong>部门信息</strong>
     *
     * @param id 主键ID
     */
    @DeleteMapping("{id}")
    public Object delete(@PathVariable String id) {
        unitService.deleteUnit(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * 删除多个 <strong>部门信息</strong>
     *
     * @param ids 主键ID列表
     */
    @DeleteMapping
    public Object deleteIds(@RequestBody List<String> ids) {
        unitService.deleteUnit(ids);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * 构建查询条件内容
     *
     * @param entity 实体对象
     * @return lambda query chain wrapper
     */
    private LambdaQueryChainWrapper<UnitEntity> buildLambdaQuery(UnitEntity entity) {
        LambdaQueryChainWrapper<UnitEntity> lambdaQuery = unitService.lambdaQuery();
        lambdaQuery.eq(StringUtils.isNotBlank(entity.getId()), UnitEntity::getId, entity.getId());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getTitle()), UnitEntity::getTitle, entity.getTitle());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getRemarks()), UnitEntity::getRemarks, entity.getRemarks());
        return lambdaQuery;
    }
}
