package city.spring.controller;

import city.spring.domain.dto.UserAccountDTO;
import city.spring.domain.entity.UserAccountEntity;
import city.spring.domain.mapper.UserAccountMapper;
import city.spring.service.UserAccountService;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户帐号控制器。
 * 用户帐号是指用户可以用来执行登录操作的帐号。
 *
 * @author HouKunLin
 * @date 2019/12/22 0022 20:32
 */
@RestController
@RequestMapping("/user/{userId}")
public class UserAccountController extends ApiController {
    /**
     * 用户帐号信息服务
     */
    private final UserAccountService userAccountService;
    /**
     * 用户帐号信息转换器
     */
    private final UserAccountMapper userAccountMapper;

    public UserAccountController(UserAccountService userAccountService, UserAccountMapper userAccountMapper) {
        this.userAccountService = userAccountService;
        this.userAccountMapper = userAccountMapper;
    }

    /**
     * 获取某个用户的账户列表。
     * 这些列表是用户可以用来的登录的登录帐号信息
     *
     * @param userId 用户ID
     * @return 结果
     */
    @GetMapping("/account")
    public R<List<UserAccountDTO>> list(@PathVariable String userId) {
        List<UserAccountEntity> userAccounts = userAccountService.getUserAccounts(userId);
        return success(userAccountMapper.toDto(userAccounts));
    }

    /**
     * 获取某个用户的某个账户信息
     *
     * @param userId    用户ID
     * @param accountId 账户ID
     * @return 结果
     */
    @GetMapping("/account/{accountId}")
    public R<UserAccountDTO> info(@PathVariable String userId, @PathVariable String accountId) {
        UserAccountEntity userAccountEntity = userAccountService.lambdaQuery()
                .eq(UserAccountEntity::getId, accountId)
                .eq(UserAccountEntity::getUserId, userId)
                .one();
        return success(userAccountMapper.toDto(userAccountEntity));
    }
}
