package city.spring.controller;

import city.spring.domain.dto.ResetPasswordDTO;
import city.spring.domain.entity.UserEntity;
import city.spring.service.UserService;
import city.spring.utils.MyBatisUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.NonNull;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户信息 控制器
 *
 * @author HouKunLin
 * @date 2019/12/20 0020 11:00
 */
@RestController
@RequestMapping("/user")
@PreAuthorize("hasAnyRole('ADMIN','user-manager') or hasAnyAuthority('user:list','user:info','user:add','user:update','user:delete','user:password')")
public class UserController extends ApiController {
    /**
     * 用户信息 Service
     */
    private final UserService userService;
    /**
     * 实体类可排序字段
     */
    private final List<SFunction<UserEntity, ?>> entityOrderFields;

    public UserController(UserService userService) {
        this.userService = userService;
        entityOrderFields = new ArrayList<>();
        entityOrderFields.add(UserEntity::getId);
        entityOrderFields.add(UserEntity::getNickname);
        entityOrderFields.add(UserEntity::getUsername);
        entityOrderFields.add(UserEntity::getPhone);
        entityOrderFields.add(UserEntity::getEmail);
        entityOrderFields.add(UserEntity::getGmtCreate);
        entityOrderFields.add(UserEntity::getGmtModified);
    }

    /**
     * 忽略对当前用登录户信息的操作
     *
     * @param user   当前登录用户
     * @param userId 前端提交的用户ID
     */
    @ModelAttribute
    public void ignoreCurrentUser(@NonNull Principal user, @PathVariable(required = false) String userId) {
        if (StringUtils.isBlank(userId)) {
            return;
        }
        if (userId.equals(user.getName())) {
            throw new RuntimeException("不能操作当前登录用户信息");
        }
    }

    /**
     * 分页获取用户列表
     *
     * @param pageable 分页信息
     */
    @PreAuthorize("hasAnyRole('ADMIN','user-manager') or hasAnyAuthority('user:list')")
    @GetMapping
    public Object list(@PageableDefault(sort = {"gmtCreate"}) Pageable pageable, UserEntity entity, @NonNull Principal user) {
        LambdaQueryChainWrapper<UserEntity> lambdaQuery = buildLambdaQuery(entity);
        MyBatisUtils.lambdaQueryAddOrder(lambdaQuery, pageable, entityOrderFields);
        // 排除当前登录用户信息
        lambdaQuery.ne(UserEntity::getId, user.getName());
        Page<UserEntity> page = lambdaQuery.page(MyBatisUtils.toPage(pageable, false));
        return success(page);
    }

    /**
     * 新增用户信息
     *
     * @param entity 用户数据对象
     */
    @PreAuthorize("hasAnyRole('ADMIN','user-manager') or hasAnyAuthority('user:add')")
    @PostMapping
    public ResponseEntity<?> save(@Validated @RequestBody UserEntity entity, BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        userService.saveUser(entity);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, entity.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    /**
     * 获取用户详细信息
     *
     * @param userId 用户ID
     */
    @PreAuthorize("hasAnyRole('ADMIN','user-manager') or hasAnyAuthority('user:info')")
    @GetMapping("/{userId}")
    public Object info(@PathVariable String userId) {
        UserEntity entity = userService.getUserInfo(userId, false, UserService.UserInfoEnum.ALL);
        return success(entity);
    }

    /**
     * 修改用户信息（目前只支持修改昵称）
     *
     * @param userId 用户ID
     * @param entity 新的用户信息
     */
    @PreAuthorize("hasAnyRole('ADMIN','user-manager') or hasAnyAuthority('user:update')")
    @PutMapping("/{userId}")
    public ResponseEntity<?> put(@PathVariable String userId, @Validated @RequestBody UserEntity entity, BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        entity.setId(userId);
        userService.updateUser(entity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 删除用户信息
     *
     * @param userId 用户ID
     */
    @PreAuthorize("hasAnyRole('ADMIN','user-manager') or hasAnyAuthority('user:delete')")
    @DeleteMapping("/{userId}")
    public ResponseEntity<?> delete(@PathVariable String userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * 删除多个 <strong>部门信息</strong>
     *
     * @param ids  主键ID列表
     * @param user 当前登录用户信息
     */
    @DeleteMapping
    public Object deleteIds(@RequestBody List<String> ids, @NonNull Principal user) {
        // 不能通过该接口删除当前登录用户信息
        ids.remove(user.getName());
        userService.deleteUser(ids);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * 修改用户密码
     *
     * @param userId           用户ID
     * @param resetPasswordDTO 新旧密码信息
     */
    @PreAuthorize("hasAnyRole('ADMIN','user-manager') or hasAnyAuthority('user:update-password')")
    @PutMapping("/{userId}/update-password")
    public ResponseEntity<?> updatePassword(@PathVariable String userId, @Validated @RequestBody ResetPasswordDTO resetPasswordDTO, BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        userService.updatePassword(userId, resetPasswordDTO.getOldPassword(), resetPasswordDTO.getNewPassword());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 重置用户密码
     *
     * @param userId   用户ID
     * @param password 新密码信息
     */
    @PreAuthorize("hasAnyRole('ADMIN','user-manager') or hasAnyAuthority('user:reset-password')")
    @PutMapping("/{userId}/reset-password")
    public ResponseEntity<?> resetPassword(@PathVariable String userId, String password) {
        if (StringUtils.isBlank(password)) {
            throw new RuntimeException("新密码不能为空");
        }
        userService.updatePassword(userId, password);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 构建查询条件内容
     *
     * @param entity 实体对象
     * @return lambda query chain wrapper
     */
    private LambdaQueryChainWrapper<UserEntity> buildLambdaQuery(UserEntity entity) {
        LambdaQueryChainWrapper<UserEntity> lambdaQuery = userService.lambdaQuery();
        lambdaQuery.eq(StringUtils.isNotBlank(entity.getId()), UserEntity::getId, entity.getId());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getNickname()), UserEntity::getNickname, entity.getNickname());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getUsername()), UserEntity::getUsername, entity.getUsername());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getPhone()), UserEntity::getPhone, entity.getPhone());
        lambdaQuery.like(StringUtils.isNotBlank(entity.getEmail()), UserEntity::getEmail, entity.getEmail());
        return lambdaQuery;
    }
}
