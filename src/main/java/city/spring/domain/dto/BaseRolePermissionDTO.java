package city.spring.domain.dto;

import lombok.Data;

import java.util.List;

/**
 * 抽象角色、权限字段信息
 *
 * @author HouKunLin
 * @date 2019/12/21 0021 21:02
 */
@Data
public abstract class BaseRolePermissionDTO {
    private List<RoleDTO> roles;
    private List<PermissionDTO> permissions;
}
