package city.spring.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 用户组信息实体类
 *
 * @author HouKunLin
 * @date 2019/9/22 0022 14:21
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GroupDTO extends BaseRolePermissionDTO implements Serializable {
    private String id;
    /**
     * 用户组名称（标题）
     */
    private String title;
    /**
     * 用户组备注信息
     */
    private String remarks;
}
