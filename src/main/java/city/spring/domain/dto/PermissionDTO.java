package city.spring.domain.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 权限信息实体类
 *
 * @author HouKunLin
 * @date 2019/9/22 0022 14:21
 */
@Data
public class PermissionDTO implements Serializable {
    private String id;
    /**
     * 权限名称（标题）
     */
    private String title;
    /**
     * 权限备注信息
     */
    private String remarks;
    /**
     * 角色代码
     */
    private String code;
}
