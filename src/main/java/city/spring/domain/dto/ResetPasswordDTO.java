package city.spring.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 修改用户密码
 *
 * @author HouKunLin
 * @date 2019/12/22 0022 20:19
 */
@Data
public class ResetPasswordDTO {
    /**
     * 旧密码
     */
    @NotBlank(message = "旧密码不能为空")
    private String oldPassword;
    /**
     * 新密码
     */
    @NotBlank(message = "新密码不能为空")
    private String newPassword;
}
