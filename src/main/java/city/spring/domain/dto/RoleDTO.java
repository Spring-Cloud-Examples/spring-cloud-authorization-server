package city.spring.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 角色信息实体类
 *
 * @author HouKunLin
 * @date 2019/9/22 0022 14:21
 */
@Data
public class RoleDTO implements Serializable {
    private String id;
    /**
     * 角色名称（标题）
     */
    private String title;
    /**
     * 角色备注信息
     */
    private String remarks;
    /**
     * 角色代码
     */
    private String code;
    /**
     * 角色所拥有的权限
     */
    private List<PermissionDTO> permissions;
}
