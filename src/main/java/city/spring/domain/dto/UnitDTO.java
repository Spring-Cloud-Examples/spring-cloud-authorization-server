package city.spring.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 部门信息实体类
 *
 * @author HouKunLin
 * @date 2019/9/22 0022 14:21
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UnitDTO extends BaseRolePermissionDTO implements Serializable {
    private String id;
    /**
     * 部门名称（标题）
     */
    private String title;
    /**
     * 部门备注信息
     */
    private String remarks;
}
