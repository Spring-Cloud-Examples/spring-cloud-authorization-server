package city.spring.domain.dto;

import city.spring.domain.enums.IdentityType;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户账户信息
 *
 * @author HouKunLin
 * @date 2019/12/2 0002 17:00
 */
@Data
public class UserAccountDTO implements Serializable {
    private String id;
    /**
     * 帐号类型：用户名、邮箱、手机号
     */
    private IdentityType identityType;
    /**
     * 帐号唯一凭据
     */
    private String identifier;
    /**
     * 当帐号类型为第三方登录时，该字段为第三方的Token信息
     */
    private String credential;
    /**
     * 当前帐号类型是否通过验证
     */
    private Boolean verified;
}
