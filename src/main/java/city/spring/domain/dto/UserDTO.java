package city.spring.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

/**
 * 返回给前端的用户信息
 *
 * @author HouKunLin
 * @date 2019/12/20 0020 16:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserDTO extends BaseRolePermissionDTO implements Serializable {
    private String id;
    private String nickname;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 冗余用户名
     */
    @Pattern(regexp = "^(|[a-zA-Z][a-zA-Z0-9_-]{3,15})$", message = "用户名4-16个字符，英文字母开头，不允许特殊符号")
    private String username;
    /**
     * 冗余手机号
     */
    @Pattern(regexp = "^(|1[0-9]{10})$", message = "手机号格式错误")
    private String phone;
    /**
     * 冗余电子邮箱
     */
    @Email(message = "电子邮箱格式错误")
    private String email;
    private Boolean isAccountNonExpired;
    private Boolean isAccountNonLocked;
    private Boolean isCredentialsNonExpired;
    private Boolean isEnabled;
    private List<UserAccountDTO> accounts;
    private List<GroupDTO> groups;
    private List<UnitDTO> units;
    /**
     * 所有权限信息
     */
    private List<GrantedAuthority> authorities;
}
