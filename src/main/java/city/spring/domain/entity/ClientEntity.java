package city.spring.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 客户端信息。
 * 使用 OAuth2 方式登录的时候需要提供一个有效的客户端信息
 *
 * @author HouKunLin
 * @date 2019/12/1 0001 22:40
 */
@Data
@EqualsAndHashCode
@TableName("client_details")
public class ClientEntity implements Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 客户端名称（标题）
     */
    private String title;
    /**
     * 客户端备注信息
     */
    private String remarks;
    /**
     * 客户端ID
     */
    private String clientId;
    /**
     * 客户端密钥
     */
    private String clientSecret;
    /**
     * 资源ID列表
     */
    private String resourceIds;
    /**
     * 范围列表
     */
    private String scope;
    /**
     * 授权的授权类型
     */
    private String authorizedGrantTypes;
    /**
     * 重定向URI
     */
    private String registeredRedirectUri;
    /**
     * 自动批准范围
     */
    private String autoApproveScopes;
    /**
     * 权限列表
     */
    private String authorities;
    /**
     * 访问令牌有效性-秒
     */
    private Integer accessTokenValiditySeconds;
    /**
     * 刷新令牌有效性-秒
     */
    private Integer refreshTokenValiditySeconds;
    /**
     * 额外的信息
     */
    private String additionalInformation;
    /**
     * 版本号
     */
    @Version
    private Integer version;
    /**
     * 数据创建时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtCreate;
    /**
     * 数据更新时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtModified;
}
