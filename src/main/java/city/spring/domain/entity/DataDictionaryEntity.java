package city.spring.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 数据字典（类别、分类）
 *
 * @author HouKunLin
 * @date 2020/3/26 0026 23:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("data_dictionary")
public class DataDictionaryEntity implements Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 数据字典名称
     */
    private String title;
    /**
     * 数据字典类型（唯一值）
     */
    private String type;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 启用状态
     */
    private Boolean enabled;
    /**
     * 版本号
     */
    @Version
    private Integer version;
    /**
     * 数据创建时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtCreate;
    /**
     * 数据更新时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtModified;
}
