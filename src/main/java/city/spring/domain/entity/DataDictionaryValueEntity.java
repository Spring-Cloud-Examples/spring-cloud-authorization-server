package city.spring.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 数据字典值信息
 *
 * @author HouKunLin
 * @date 2020/3/26 0026 23:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("data_dictionary_value")
public class DataDictionaryValueEntity implements Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 关联 数据字典 对象（数据字典的主键ID）
     */
    private String dictionaryId;
    /**
     * 字段名称
     */
    private String label;
    /**
     * 字段值
     */
    private String value;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 启用状态
     */
    private Boolean enabled;
    /**
     * 数据字典值列表排序值
     */
    private Integer sorted;
    /**
     * 版本号
     */
    @Version
    private Integer version;
    /**
     * 数据创建时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtCreate;
    /**
     * 数据更新时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtModified;
}
