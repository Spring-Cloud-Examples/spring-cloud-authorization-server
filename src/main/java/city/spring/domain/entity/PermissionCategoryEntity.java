package city.spring.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 权限信息类别实体类
 *
 * @author HouKunLin
 * @date 2020-03-26 09:31:25
 */
@Data
@TableName("permission_category")
public class PermissionCategoryEntity implements Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 权限信息类别名称（标题）
     */
    private String title;
    /**
     * 权限信息类别备注信息
     */
    private String remarks;
    /**
     * 该权限类别下的所有权限信息
     */
    @TableField(exist = false)
    private List<PermissionEntity> permissions;
    /**
     * 版本号
     */
    @Version
    private Integer version;
    /**
     * 数据创建时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtCreate;
    /**
     * 数据更新时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtModified;

    public PermissionCategoryEntity() {
    }
}
