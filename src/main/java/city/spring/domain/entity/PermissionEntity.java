package city.spring.domain.entity;

import city.spring.utils.RoleUtils;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Date;

/**
 * 权限信息实体类
 *
 * @author HouKunLin
 * @date 2019/9/22 0022 14:21
 */
@Data
@TableName("permission")
public class PermissionEntity implements GrantedAuthority, Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 关联权限信息类别ID
     */
    private String categoryId;
    /**
     * 权限名称（标题）
     */
    private String title;
    /**
     * 权限备注信息
     */
    private String remarks;
    /**
     * 角色代码
     */
    private String code;
    /**
     * 版本号
     */
    @Version
    private Integer version;
    /**
     * 数据创建时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtCreate;
    /**
     * 数据更新时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtModified;

    public PermissionEntity() {
    }

    public PermissionEntity(String code, String title) {
        this.code = RoleUtils.getAuthorityRemoveDefaultPrefix(code);
        this.title = title;
    }

    @Override
    public String getAuthority() {
        return code;
    }

    public void setCode(String code) {
        this.code = RoleUtils.getAuthorityRemoveDefaultPrefix(code);
    }
}
