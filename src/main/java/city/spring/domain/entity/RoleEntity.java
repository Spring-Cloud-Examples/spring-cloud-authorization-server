package city.spring.domain.entity;

import city.spring.utils.RoleUtils;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 角色信息实体类
 *
 * @author HouKunLin
 * @date 2019/9/22 0022 14:21
 */
@Data
@TableName("role")
public class RoleEntity implements GrantedAuthority, Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 角色名称（标题）
     */
    private String title;
    /**
     * 角色备注信息
     */
    private String remarks;
    /**
     * 角色代码（数据库中存储的代码不包含角色前缀信息）
     */
    private String code;
    /**
     * 角色所拥有的权限列表
     */
    @TableField(exist = false)
    private List<PermissionEntity> permissions;
    /**
     * 版本号
     */
    @Version
    private Integer version;
    /**
     * 数据创建时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtCreate;
    /**
     * 数据更新时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtModified;

    public RoleEntity() {
    }

    public RoleEntity(String code, String title) {
        this.code = RoleUtils.getAuthorityRemoveDefaultPrefix(code);
        this.title = title;
    }

    @Override
    public String getAuthority() {
        // 这是一个角色，返回角色代码时必须保证角色代码含有角色前缀信息
        return RoleUtils.getAuthorityWithDefaultPrefix(code);
    }

    public void setCode(String code) {
        this.code = RoleUtils.getAuthorityRemoveDefaultPrefix(code);
    }
}
