package city.spring.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 部门信息实体类
 *
 * @author HouKunLin
 * @date 2019/9/22 0022 14:21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("unit")
public class UnitEntity extends BaseRolePermissionEntity implements Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 部门名称（标题）
     */
    private String title;
    /**
     * 部门备注信息
     */
    private String remarks;
    /**
     * 版本号
     */
    @Version
    private Integer version;
    /**
     * 数据创建时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtCreate;
    /**
     * 数据更新时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtModified;

    public UnitEntity() {
    }
}
