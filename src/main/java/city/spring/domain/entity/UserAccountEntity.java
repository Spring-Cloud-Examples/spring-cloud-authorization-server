package city.spring.domain.entity;

import city.spring.domain.enums.IdentityType;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户账户信息
 *
 * @author HouKunLin
 * @date 2019/12/2 0002 17:00
 */
@Data
@TableName("user_account")
public class UserAccountEntity implements Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 关联用户ID
     */
    private String userId;
    /**
     * 帐号类型：用户名、邮箱、手机号
     */
    private IdentityType identityType;
    /**
     * 帐号唯一凭据
     */
    private String identifier;
    /**
     * 当帐号类型为第三方登录时，该字段为第三方的Token信息
     */
    private String credential;
    /**
     * 当前帐号类型是否通过验证
     */
    @TableField(value = "is_verified")
    private Boolean verified;

    /**
     * 版本号
     */
    @Version
    private Integer version;
    /**
     * 数据创建时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtCreate;
    /**
     * 数据更新时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtModified;
}
