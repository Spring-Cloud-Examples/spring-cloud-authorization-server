package city.spring.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 用户信息实体类
 *
 * @author HouKunLin
 * @date 2019/7/21 0021 17:41
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("user")
public class UserEntity extends BaseRolePermissionEntity implements Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 用户昵称
     */
    private String nickname;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 冗余用户名
     */
    private String username;
    /**
     * 冗余手机号
     */
    private String phone;
    /**
     * 冗余电子邮箱
     */
    private String email;
    /**
     * 用户的密码信息
     */
    private String password;
    /**
     * 用户所在用户组
     */
    @TableField(exist = false)
    private List<GroupEntity> groups;
    /**
     * 用户所在用户部门
     */
    @TableField(exist = false)
    private List<UnitEntity> units;
    /**
     * 当前用户的所有登录帐号信息
     */
    @TableField(exist = false)
    private List<UserAccountEntity> accounts;
    /**
     * 帐户未过期
     */
    private Boolean isAccountNonExpired;
    /**
     * 帐户未锁定
     */
    private Boolean isAccountNonLocked;
    /**
     * 凭证未过期
     */
    private Boolean isCredentialsNonExpired;
    /**
     * 启用
     */
    private Boolean isEnabled;
    /**
     * 版本号
     */
    @Version
    private Integer version;
    /**
     * 数据创建时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtCreate;
    /**
     * 数据更新时间
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Date gmtModified;

    public UserEntity() {

    }

    @Override
    public Set<String> getAllAuthorities() {
        // 收集用户的角色和权限信息
        Set<String> allAuthorities = super.getAllAuthorities();
        // 收集用户所属群组的角色和权限信息
        collectionAll(allAuthorities, groups);
        // 收集用户所属部门的角色和权限信息
        collectionAll(allAuthorities, units);
        return allAuthorities;
    }

}
