package city.spring.domain.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * 登录帐号类型
 *
 * @author HouKunLin
 * @date 2019/12/2 0002 17:12
 */
public enum IdentityType implements IEnum<Integer> {
    /**
     * 用户名
     */
    USERNAME(1, "用户名", true),
    EMAIL(2, "电子邮件", true),
    PHONE(3, "手机号", true),
    WeiXin(4, "微信", false),
    QQ(5, "腾讯QQ", false),
    ;

    private final Integer value;
    private final String title;
    private final boolean isPassword;

    IdentityType(Integer value, String title, boolean isPassword) {
        this.value = value;
        this.title = title;
        this.isPassword = isPassword;
    }

    /**
     * 前端直接指定code字段传给后端，而不是使用TEXT值
     *
     * @param code 类型值
     * @return 登录帐号类型
     */
    @JsonCreator
    public static IdentityType getItem(int code) {
        for (IdentityType item : values()) {
            Integer value = item.getValue();
            if (value.equals(code)) {
                return item;
            }
        }
        return null;
    }

    public static List<IdentityType> getIdentityType(boolean needPassword) {
        IdentityType[] values = IdentityType.values();
        List<IdentityType> identityTypes = new ArrayList<>();
        for (IdentityType value : values) {
            if (value.isPassword == needPassword) {
                identityTypes.add(value);
            }
        }
        return identityTypes;
    }

    @JsonValue
    @Override
    public Integer getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }

    public boolean isPassword() {
        return isPassword;
    }
}
