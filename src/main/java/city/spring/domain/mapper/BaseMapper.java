package city.spring.domain.mapper;

import java.util.List;

/**
 * DTO、Entity互相转换
 *
 * @author HouKunLin
 * @date 2019/12/21 0021 21:23
 */
public interface BaseMapper<DTO, ENTITY> {

    /**
     * DTO转Entity
     *
     * @param dto DTO
     * @return ENTITY
     */
    ENTITY toEntity(DTO dto);

    /**
     * DTO集合转Entity集合
     *
     * @param dtoList DTO集合
     * @return ENTITY集合
     */
    List<ENTITY> toEntity(List<DTO> dtoList);

    /**
     * Entity转DTO
     *
     * @param entity ENTITY
     * @return DTO
     */
    DTO toDto(ENTITY entity);

    /**
     * Entity集合转DTO集合
     *
     * @param entityList ENTITY集合
     * @return DTO集合
     */
    List<DTO> toDto(List<ENTITY> entityList);
}
