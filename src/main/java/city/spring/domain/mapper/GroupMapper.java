package city.spring.domain.mapper;

import city.spring.domain.dto.GroupDTO;
import city.spring.domain.entity.GroupEntity;
import org.mapstruct.Mapper;

/**
 * 用户帐号DTO、VO、BO转换
 *
 * @author HouKunLin
 * @date 2019/12/21 0021 21:06
 */
@Mapper(componentModel = "spring", uses = {RoleMapper.class, PermissionMapper.class})
public interface GroupMapper extends BaseMapper<GroupDTO, GroupEntity> {
}
