package city.spring.domain.mapper;

import city.spring.domain.dto.PermissionDTO;
import city.spring.domain.entity.PermissionEntity;
import org.mapstruct.Mapper;

/**
 * 用户帐号DTO、VO、BO转换
 *
 * @author HouKunLin
 * @date 2019/12/21 0021 21:06
 */
@Mapper(componentModel = "spring")
public interface PermissionMapper extends BaseMapper<PermissionDTO, PermissionEntity> {
}
