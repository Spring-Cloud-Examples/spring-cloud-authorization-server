package city.spring.domain.mapper;

import city.spring.domain.dto.UnitDTO;
import city.spring.domain.entity.UnitEntity;
import org.mapstruct.Mapper;

/**
 * 转换DTO、VO、BO
 *
 * @author HouKunLin
 * @date 2019/12/20 0020 17:32
 */
@Mapper(componentModel = "spring", uses = {RoleMapper.class, PermissionMapper.class})
public interface UnitMapper extends BaseMapper<UnitDTO, UnitEntity> {
}
