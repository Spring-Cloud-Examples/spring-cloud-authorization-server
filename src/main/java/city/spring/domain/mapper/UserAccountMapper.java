package city.spring.domain.mapper;

import city.spring.domain.dto.UserAccountDTO;
import city.spring.domain.entity.UserAccountEntity;
import org.mapstruct.Mapper;

/**
 * 用户帐号DTO、VO、BO转换
 *
 * @author HouKunLin
 * @date 2019/12/21 0021 21:06
 */
@Mapper(componentModel = "spring")
public interface UserAccountMapper extends BaseMapper<UserAccountDTO, UserAccountEntity> {
}
