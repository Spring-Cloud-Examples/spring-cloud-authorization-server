package city.spring.domain.mapper;

import city.spring.domain.dto.UserDTO;
import city.spring.domain.entity.UserEntity;
import org.mapstruct.Mapper;

/**
 * 转换DTO、VO、BO
 *
 * @author HouKunLin
 * @date 2019/12/20 0020 17:32
 */
@Mapper(componentModel = "spring", uses = {UserAccountMapper.class, GroupMapper.class, UnitMapper.class, RoleMapper.class, PermissionMapper.class})
public interface UserMapper extends BaseMapper<UserDTO, UserEntity> {
}
