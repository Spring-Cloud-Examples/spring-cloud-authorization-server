package city.spring.modules.ext.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 关联关系： group - permission
 *
 * @author HouKunLin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user_group_permission_ext")
public class GroupPermissionExt implements Serializable {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String groupId;
    private String relatedId;
}