package city.spring.modules.ext.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 关联关系： group - role
 *
 * @author HouKunLin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user_group_role_ext")
public class GroupRoleExt implements Serializable {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String groupId;
    private String relatedId;
}