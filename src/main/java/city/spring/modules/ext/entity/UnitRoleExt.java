package city.spring.modules.ext.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 关联关系： unit - role
 *
 * @author HouKunLin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("unit_role_ext")
public class UnitRoleExt implements Serializable {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String unitId;
    private String relatedId;
}