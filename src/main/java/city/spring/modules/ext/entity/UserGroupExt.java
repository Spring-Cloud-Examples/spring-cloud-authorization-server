package city.spring.modules.ext.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 关联关系： user - group
 *
 * @author HouKunLin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user_group_ext")
public class UserGroupExt implements Serializable {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String userId;
    private String relatedId;
}