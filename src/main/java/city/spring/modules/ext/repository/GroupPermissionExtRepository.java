package city.spring.modules.ext.repository;

import city.spring.modules.ext.entity.GroupPermissionExt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 关联关系：用户组-权限
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:10
 */
@Repository
public interface GroupPermissionExtRepository extends BaseMapper<GroupPermissionExt> {
}