package city.spring.modules.ext.repository;

import city.spring.modules.ext.entity.RolePermissionExt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 关联关系：角色-权限
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:11
 */
@Repository
public interface RolePermissionExtRepository extends BaseMapper<RolePermissionExt> {
}