package city.spring.modules.ext.repository;

import city.spring.modules.ext.entity.UnitRoleExt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 关联关系：部门-角色
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:12
 */
@Repository
public interface UnitRoleExtRepository extends BaseMapper<UnitRoleExt> {
}