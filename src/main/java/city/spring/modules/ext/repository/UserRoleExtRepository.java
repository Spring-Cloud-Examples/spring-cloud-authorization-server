package city.spring.modules.ext.repository;

import city.spring.modules.ext.entity.UserRoleExt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 关联关系：用户-角色
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:13
 */
@Repository
public interface UserRoleExtRepository extends BaseMapper<UserRoleExt> {
}