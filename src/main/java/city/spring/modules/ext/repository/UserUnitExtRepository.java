package city.spring.modules.ext.repository;

import city.spring.modules.ext.entity.UserUnitExt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 关联关系：用户-部门
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:14
 */
@Repository
public interface UserUnitExtRepository extends BaseMapper<UserUnitExt> {
}