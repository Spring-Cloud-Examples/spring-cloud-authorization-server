package city.spring.modules.ext.service;

import city.spring.modules.ext.entity.GroupPermissionExt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 关联关系：用户组-权限
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:10
 */
public interface GroupPermissionExtService extends IService<GroupPermissionExt> {
}