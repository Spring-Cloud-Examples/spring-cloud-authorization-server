package city.spring.modules.ext.service;

import city.spring.modules.ext.entity.RolePermissionExt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 关联关系：角色-权限
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:11
 */
public interface RolePermissionExtService extends IService<RolePermissionExt> {
}