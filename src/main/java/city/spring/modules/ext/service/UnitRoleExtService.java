package city.spring.modules.ext.service;

import city.spring.modules.ext.entity.UnitRoleExt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 关联关系：部门-角色
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:12
 */
public interface UnitRoleExtService extends IService<UnitRoleExt> {
}