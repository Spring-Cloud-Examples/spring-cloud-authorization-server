package city.spring.modules.ext.service;

import city.spring.modules.ext.entity.UserGroupExt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 关联关系：用户-用户组
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:13
 */
public interface UserGroupExtService extends IService<UserGroupExt> {
}