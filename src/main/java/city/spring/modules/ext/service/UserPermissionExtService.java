package city.spring.modules.ext.service;

import city.spring.modules.ext.entity.UserPermissionExt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 关联关系：用户-权限
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:13
 */
public interface UserPermissionExtService extends IService<UserPermissionExt> {
}