package city.spring.modules.ext.service;

import city.spring.modules.ext.entity.UserRoleExt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 关联关系：用户-角色
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:13
 */
public interface UserRoleExtService extends IService<UserRoleExt> {
}