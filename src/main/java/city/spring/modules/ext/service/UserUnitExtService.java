package city.spring.modules.ext.service;

import city.spring.modules.ext.entity.UserUnitExt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 关联关系：用户-部门
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:14
 */
public interface UserUnitExtService extends IService<UserUnitExt> {
}