package city.spring.modules.ext.service.impl;

import city.spring.modules.ext.entity.GroupPermissionExt;
import city.spring.modules.ext.repository.GroupPermissionExtRepository;
import city.spring.modules.ext.service.GroupPermissionExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 关联关系：用户组-权限
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:10
 */
@Service
public class GroupPermissionExtServiceImpl extends ServiceImpl<GroupPermissionExtRepository, GroupPermissionExt> implements GroupPermissionExtService {
}