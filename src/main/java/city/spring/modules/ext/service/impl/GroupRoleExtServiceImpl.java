package city.spring.modules.ext.service.impl;

import city.spring.modules.ext.entity.GroupRoleExt;
import city.spring.modules.ext.repository.GroupRoleExtRepository;
import city.spring.modules.ext.service.GroupRoleExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 关联关系：用户组-角色
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:10
 */
@Service
public class GroupRoleExtServiceImpl extends ServiceImpl<GroupRoleExtRepository, GroupRoleExt> implements GroupRoleExtService {
}