package city.spring.modules.ext.service.impl;

import city.spring.modules.ext.entity.RolePermissionExt;
import city.spring.modules.ext.repository.RolePermissionExtRepository;
import city.spring.modules.ext.service.RolePermissionExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 关联关系：角色-权限
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:11
 */
@Service
public class RolePermissionExtServiceImpl extends ServiceImpl<RolePermissionExtRepository, RolePermissionExt> implements RolePermissionExtService {
}