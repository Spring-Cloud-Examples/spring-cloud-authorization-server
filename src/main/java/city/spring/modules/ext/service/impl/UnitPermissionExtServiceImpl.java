package city.spring.modules.ext.service.impl;

import city.spring.modules.ext.entity.UnitPermissionExt;
import city.spring.modules.ext.repository.UnitPermissionExtRepository;
import city.spring.modules.ext.service.UnitPermissionExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 关联关系：部门-权限
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:12
 */
@Service
public class UnitPermissionExtServiceImpl extends ServiceImpl<UnitPermissionExtRepository, UnitPermissionExt> implements UnitPermissionExtService {
}