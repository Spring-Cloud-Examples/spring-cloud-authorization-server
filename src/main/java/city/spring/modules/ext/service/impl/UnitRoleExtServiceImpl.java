package city.spring.modules.ext.service.impl;

import city.spring.modules.ext.entity.UnitRoleExt;
import city.spring.modules.ext.repository.UnitRoleExtRepository;
import city.spring.modules.ext.service.UnitRoleExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 关联关系：部门-角色
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:12
 */
@Service
public class UnitRoleExtServiceImpl extends ServiceImpl<UnitRoleExtRepository, UnitRoleExt> implements UnitRoleExtService {
}