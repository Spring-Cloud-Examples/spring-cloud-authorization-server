package city.spring.modules.ext.service.impl;

import city.spring.modules.ext.entity.UserGroupExt;
import city.spring.modules.ext.repository.UserGroupExtRepository;
import city.spring.modules.ext.service.UserGroupExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 关联关系：用户-用户组
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:13
 */
@Service
public class UserGroupExtServiceImpl extends ServiceImpl<UserGroupExtRepository, UserGroupExt> implements UserGroupExtService {
}