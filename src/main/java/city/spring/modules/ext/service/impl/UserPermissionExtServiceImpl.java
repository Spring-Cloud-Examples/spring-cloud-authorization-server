package city.spring.modules.ext.service.impl;

import city.spring.modules.ext.entity.UserPermissionExt;
import city.spring.modules.ext.repository.UserPermissionExtRepository;
import city.spring.modules.ext.service.UserPermissionExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 关联关系：用户-权限
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:13
 */
@Service
public class UserPermissionExtServiceImpl extends ServiceImpl<UserPermissionExtRepository, UserPermissionExt> implements UserPermissionExtService {
}