package city.spring.modules.ext.service.impl;

import city.spring.modules.ext.entity.UserRoleExt;
import city.spring.modules.ext.repository.UserRoleExtRepository;
import city.spring.modules.ext.service.UserRoleExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 关联关系：用户-角色
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:13
 */
@Service
public class UserRoleExtServiceImpl extends ServiceImpl<UserRoleExtRepository, UserRoleExt> implements UserRoleExtService {
}