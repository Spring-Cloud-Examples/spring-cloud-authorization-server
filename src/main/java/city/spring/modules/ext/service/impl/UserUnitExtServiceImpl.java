package city.spring.modules.ext.service.impl;

import city.spring.modules.ext.entity.UserUnitExt;
import city.spring.modules.ext.repository.UserUnitExtRepository;
import city.spring.modules.ext.service.UserUnitExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 关联关系：用户-部门
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 18:14
 */
@Service
public class UserUnitExtServiceImpl extends ServiceImpl<UserUnitExtRepository, UserUnitExt> implements UserUnitExtService {
}