package city.spring.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Configuration;

/**
 * 消息中间件配置
 *
 * @author HouKunLin
 * @date 2019/12/24 0024 16:35
 */
@Configuration
@EnableBinding(SmsNoticeTopic.class)
public class MqConfiguration {
    private final static Logger logger = LoggerFactory.getLogger(MqConfiguration.class);

    @StreamListener(SmsNoticeTopic.INPUT)
    public void receive(String payload) {
        logger.info("处理修改密码时的短信通知: " + payload);
        // TODO 处理修改密码时的短信通知
        // throw new RuntimeException("BOOM!");
    }
}
