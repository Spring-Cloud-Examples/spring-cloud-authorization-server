package city.spring.mq;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * 短信通知
 * Spring Cloud Stream 如何消费自己生产的消息： http://blog.didispace.com/spring-cloud-starter-finchley-7-1/
 * 在消费自己生产的消息的时候，需要修改通道名，使用不同的名字，然后在配置文件中，为这两个通道设置相同的Topic名称，例如
 * <code>
 * spring.cloud.stream.bindings.base-sms-topic-output.destination=base-sms-topic
 * spring.cloud.stream.bindings.base-sms-topic-input.destination=base-sms-topic
 * </code>
 *
 * @author HouKunLin
 * @date 2019/12/24 0024 16:02
 */
public interface SmsNoticeTopic {
    /**
     * 定义基础服务的短信消息输出通道
     */
    String OUTPUT = "base-sms-topic-output";
    /**
     * 定义基础服务的短信消息输入通道
     */
    String INPUT = "base-sms-topic-input";

    /**
     * 发送短信消息通知
     *
     * @return MessageChannel
     */
    @Output(OUTPUT)
    MessageChannel output();

    /**
     * 接收短信消息通知
     *
     * @return SubscribableChannel
     */
    @Input(INPUT)
    SubscribableChannel input();
}
