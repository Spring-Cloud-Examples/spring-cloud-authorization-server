package city.spring.repository;

import city.spring.domain.entity.ClientEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 客户详细信息存储库
 *
 * @author HouKunLin
 * @date 2019-12-2 16:53:20
 */
@Repository
public interface ClientRepository extends BaseMapper<ClientEntity> {
}
