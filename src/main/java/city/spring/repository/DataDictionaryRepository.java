package city.spring.repository;

import city.spring.domain.entity.DataDictionaryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 数据字典类型存储库
 *
 * @author HouKunLin
 * @date 2020/3/26 0026 23:34
 */
@Repository
public interface DataDictionaryRepository extends BaseMapper<DataDictionaryEntity> {
}
