package city.spring.repository;

import city.spring.domain.entity.DataDictionaryValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 数据字典值信息存储库
 *
 * @author HouKunLin
 * @date 2020/3/26 0026 23:35
 */
@Repository
public interface DataDictionaryValueRepository extends BaseMapper<DataDictionaryValueEntity> {
}
