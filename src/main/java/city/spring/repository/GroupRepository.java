package city.spring.repository;

import city.spring.domain.entity.GroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户组信息存储库
 *
 * @author HouKunLin
 * @date 2019-12-2 16:53:20
 */
@Repository
public interface GroupRepository extends BaseMapper<GroupEntity> {
    /**
     * 获取用户的用户组列表
     *
     * @param userId 用户ID
     * @return 用户组列表
     */
    List<GroupEntity> getUserGroups(@Param("userId") String userId);
}
