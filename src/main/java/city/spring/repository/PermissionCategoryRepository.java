package city.spring.repository;

import city.spring.domain.entity.PermissionCategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 权限信息类别存储库
 *
 * @author HouKunLin
 * @date 2020-03-26 09:31:25
 */
@Repository
public interface PermissionCategoryRepository extends BaseMapper<PermissionCategoryEntity> {
}
