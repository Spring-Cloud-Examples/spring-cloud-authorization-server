package city.spring.repository;

import city.spring.domain.entity.PermissionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 权限信息存储库
 *
 * @author HouKunLin
 * @date 2019-12-2 16:53:20
 */
@Repository
public interface PermissionRepository extends BaseMapper<PermissionEntity> {
    /**
     * 获取用户的权限列表
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    List<PermissionEntity> getUserPermissions(@Param("userId") String userId);

    /**
     * 获取角色的权限列表
     *
     * @param roleId 角色ID
     * @return 权限列表
     */
    List<PermissionEntity> getRolePermissions(@Param("roleId") String roleId);

    /**
     * 获取用户组的权限列表
     *
     * @param groupId 用户组ID
     * @return 权限列表
     */
    List<PermissionEntity> getGroupPermissions(@Param("groupId") String groupId);

    /**
     * 获取部门的权限列表
     *
     * @param unitId 部门ID
     * @return 权限列表
     */
    List<PermissionEntity> getUnitPermissions(@Param("unitId") String unitId);
}
