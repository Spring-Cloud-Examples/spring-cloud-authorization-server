package city.spring.repository;

import city.spring.domain.entity.RoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色信息存储库
 *
 * @author HouKunLin
 * @date 2019-12-2 16:53:20
 */
@Repository
public interface RoleRepository extends BaseMapper<RoleEntity> {
    /**
     * 获取用户的角色列表
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    List<RoleEntity> getUserRoles(@Param("userId") String userId);

    /**
     * 获取用户组的角色列表
     *
     * @param groupId 用户组ID
     * @return 角色列表
     */
    List<RoleEntity> getGroupRoles(@Param("groupId") String groupId);

    /**
     * 获取部门的角色列表
     *
     * @param unitId 部门ID
     * @return 角色列表
     */
    List<RoleEntity> getUnitRoles(@Param("unitId") String unitId);
}
