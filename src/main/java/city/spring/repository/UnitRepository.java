package city.spring.repository;

import city.spring.domain.entity.UnitEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 部门信息存储库
 *
 * @author HouKunLin
 * @date 2019-12-2 16:53:20
 */
@Repository
public interface UnitRepository extends BaseMapper<UnitEntity> {
    /**
     * 获取用户的部门列表
     *
     * @param userId 用户ID
     * @return 部门列表
     */
    List<UnitEntity> getUserUnits(@Param("userId") String userId);
}
