package city.spring.repository;

import city.spring.domain.entity.UserAccountEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 用户帐号信息存储库
 *
 * @author HouKunLin
 * @date 2019-12-2 16:53:20
 */
@Repository
public interface UserAccountRepository extends BaseMapper<UserAccountEntity> {
}
