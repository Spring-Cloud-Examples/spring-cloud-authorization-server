package city.spring.repository;

import city.spring.domain.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 用户信息存储库
 *
 * @author HouKunLin
 * @date 2019-12-2 16:53:20
 */
@Repository
public interface UserRepository extends BaseMapper<UserEntity> {
    /**
     * 通过用户ID主键查找用户，或者通过用户登录帐号唯一登录凭据查找用户
     *
     * @param userIdOrIdentifier 用户ID、用户帐号登录凭据
     * @return 用户信息
     */
    UserEntity findByUserIdOrAccountIdentifier(@Param("userIdOrIdentifier") String userIdOrIdentifier);

    /**
     * 查找用户帐号的用户信息，条件：用户名（用户信息表、用户账户信息表）
     *
     * @param username 用户名
     * @return 用户信息
     */
    UserEntity findByUsernameAccount(@Param("username") String username);

    /**
     * 查找用户帐号的用户信息，条件：电子邮件（用户信息表、用户账户信息表）
     *
     * @param email 电子邮件
     * @return 用户信息
     */
    UserEntity findByEmailAccount(@Param("email") String email);

    /**
     * 查找用户帐号的用户信息，条件：手机号（用户信息表、用户账户信息表）
     *
     * @param phone 手机号
     * @return 用户信息
     */
    UserEntity findByPhoneAccount(@Param("phone") String phone);

    /**
     * 修改用户密码
     *
     * @param userId      用户ID
     * @param newPassword 新密码
     */
    void updatePassword(@Param("userId") String userId, @Param("newPassword") String newPassword);
}
