package city.spring.service;

import city.spring.domain.entity.ClientEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 客户端信息服务
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 15:14
 */
public interface ClientService extends IService<ClientEntity> {
    /**
     * 缓存存储名称
     */
    String CACHE_NAME = "client_details";
}
