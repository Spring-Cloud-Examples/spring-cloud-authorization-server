package city.spring.service;

import city.spring.domain.entity.DataDictionaryEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 数据字典类型 Service
 *
 * @author HouKunLin
 * @date 2020/3/26 0026 23:37
 */
public interface DataDictionaryService extends IService<DataDictionaryEntity> {
    /**
     * 缓存存储名称
     */
    String CACHE_NAME = "data_dictionary_type";

    /**
     * 业务处理：保存一个数据字典类型
     *
     * @param entity 数据字典类型
     */
    void saveDataDictionaryType(DataDictionaryEntity entity);

    /**
     * 业务处理：修改一个数据字典类型
     *
     * @param entity 数据字典类型
     */
    void updateDataDictionaryType(DataDictionaryEntity entity);

    /**
     * 业务处理：删除一个数据字典类型
     *
     * @param primaryKey 主键ID
     */
    void deleteDataDictionaryType(String primaryKey);

    /**
     * 业务处理：删除多个数据字典类型
     *
     * @param primaryKeys 主键ID列表
     */
    void deleteDataDictionaryType(List<String> primaryKeys);

    /**
     * 设置一个数据字典类型的启用状态
     *
     * @param primaryKey 数据字典类型的主键ID
     * @param enabled    启用状态
     */
    void enabled(String primaryKey, boolean enabled);
}
