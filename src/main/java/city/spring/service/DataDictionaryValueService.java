package city.spring.service;

import city.spring.domain.entity.DataDictionaryValueEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 数据字典值信息 Service
 *
 * @author HouKunLin
 * @date 2020/3/26 0026 23:36
 */
public interface DataDictionaryValueService extends IService<DataDictionaryValueEntity> {
    /**
     * 缓存存储名称
     */
    String CACHE_NAME = "data_dictionary_value";

    /**
     * 业务处理：保存一个数据字典值信息
     *
     * @param entity 数据字典值信息
     */
    void saveDataDictionaryValue(DataDictionaryValueEntity entity);

    /**
     * 业务处理：修改一个数据字典值信息
     *
     * @param entity 数据字典值信息
     */
    void updateDataDictionaryValue(DataDictionaryValueEntity entity);

    /**
     * 业务处理：删除一个数据字典值信息
     *
     * @param primaryKey 主键ID
     */
    void deleteDataDictionaryValue(String primaryKey);

    /**
     * 业务处理：删除多个数据字典值信息
     *
     * @param primaryKeys 主键ID列表
     */
    void deleteDataDictionaryValue(List<String> primaryKeys);

    /**
     * 设置一个数据字典值的启用状态
     *
     * @param primaryKey 数据字典值的主键ID
     * @param enabled    启用状态
     */
    void enabled(String primaryKey, boolean enabled);
}
