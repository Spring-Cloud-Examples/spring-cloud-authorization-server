package city.spring.service;

import city.spring.domain.entity.GroupEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 用户组信息服务
 *
 * @author HouKunLin
 * @date 2019/12/8 0008 14:12
 */
public interface GroupService extends IService<GroupEntity> {
    /**
     * 获取用户的用户组列表（包含权限信息）
     *
     * @param userId 用户ID
     * @return 用户组列表
     */
    List<GroupEntity> getUserGroupsWithAuthority(String userId);

    /**
     * 获取用户的用户组信息（不含权限信息）
     *
     * @param userId 用户ID
     * @return 用户组列表
     */
    List<GroupEntity> getUserGroups(String userId);

    /**
     * 加载权限信息
     *
     * @param entity 用户组
     */
    void loadPermissions(GroupEntity entity);

    /**
     * 加载权限信息
     *
     * @param entities 用户组列表
     */
    void loadPermissions(List<GroupEntity> entities);

    /**
     * 加载角色信息
     *
     * @param entity 用户组
     */
    void loadRoles(GroupEntity entity);

    /**
     * 加载角色信息
     *
     * @param entities 用户组列表
     */
    void loadRoles(List<GroupEntity> entities);

    /**
     * 通过主键ID获取一个用户组信息
     *
     * @param primaryKey      用户组主键ID
     * @param loadPermissions 是否加载用户组的特殊权限列表
     * @param loadRoles       是否加载用户组的角色列表
     * @return 用户组信息 group info
     */
    GroupEntity getGroupInfo(String primaryKey, boolean loadPermissions, boolean loadRoles);

    /**
     * 业务处理：保存一个用户组信息
     *
     * @param entity 用户组信息
     */
    void saveGroup(GroupEntity entity);

    /**
     * 业务处理：修改一个用户组信息
     *
     * @param entity 用户组信息
     */
    void updateGroup(GroupEntity entity);

    /**
     * 业务处理：删除一个用户组信息
     *
     * @param primaryKey 主键ID
     */
    void deleteGroup(String primaryKey);

    /**
     * 业务处理：删除多个用户组信息
     *
     * @param primaryKeys 主键ID列表
     */
    void deleteGroup(List<String> primaryKeys);
}
