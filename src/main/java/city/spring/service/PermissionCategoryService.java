package city.spring.service;

import city.spring.domain.entity.PermissionCategoryEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 权限信息类别服务
 *
 * @author HouKunLin
 * @date 2020-03-26 09:31:25
 */
public interface PermissionCategoryService extends IService<PermissionCategoryEntity> {
    String CACHE_NAME = "permission_category";
}
