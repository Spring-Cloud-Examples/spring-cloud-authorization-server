package city.spring.service;

import city.spring.domain.entity.PermissionEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 权限信息服务
 *
 * @author HouKunLin
 * @date 2019/12/8 0008 14:12
 */
public interface PermissionService extends IService<PermissionEntity> {
    String CACHE_NAME = "permission";

    /**
     * 获取用户的权限列表
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    List<PermissionEntity> getUserPermissions(String userId);

    /**
     * 获取角色的权限列表
     *
     * @param roleId 角色ID
     * @return 权限列表
     */
    List<PermissionEntity> getRolePermissions(String roleId);

    /**
     * 获取用户组的权限列表
     *
     * @param groupId 用户组ID
     * @return 权限列表
     */
    List<PermissionEntity> getGroupPermissions(String groupId);

    /**
     * 获取部门的权限列表
     *
     * @param unitId 部门ID
     * @return 权限列表
     */
    List<PermissionEntity> getUnitPermissions(String unitId);

    /**
     * 业务处理：保存一个权限信息
     *
     * @param entity 权限信息
     */
    void savePermission(PermissionEntity entity);

    /**
     * 业务处理：修改一个权限信息
     *
     * @param entity 权限信息
     */
    void updatePermission(PermissionEntity entity);

    /**
     * 业务处理：删除一个权限信息
     *
     * @param primaryKey 主键ID
     */
    void deletePermission(String primaryKey);

    /**
     * 业务处理：删除多个权限信息
     *
     * @param primaryKeys 主键ID列表
     */
    void deletePermission(List<String> primaryKeys);

}
