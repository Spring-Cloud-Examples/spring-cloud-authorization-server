package city.spring.service;

import city.spring.domain.entity.RoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 角色信息服务
 *
 * @author HouKunLin
 * @date 2019/12/8 0008 14:12
 */
public interface RoleService extends IService<RoleEntity> {
    String CACHE_NAME = "role";
    /**
     * 获取用户的角色列表（包含权限）
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    List<RoleEntity> getUserRolesWithAuthority(String userId);

    /**
     * 获取用户组的角色列表（包含权限）
     *
     * @param groupId 用户组ID
     * @return 角色列表
     */
    List<RoleEntity> getGroupRolesWithAuthority(String groupId);

    /**
     * 获取部门的角色列表（包含权限）
     *
     * @param unitId 部门ID
     * @return 角色列表
     */
    List<RoleEntity> getUnitRolesWithAuthority(String unitId);

    /**
     * 获取用户的角色列表（不含权限）
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    List<RoleEntity> getUserRoles(String userId);

    /**
     * 获取用户组的角色列表（不含权限）
     *
     * @param groupId 用户组ID
     * @return 角色列表
     */
    List<RoleEntity> getGroupRoles(String groupId);

    /**
     * 获取部门的角色列表（不含权限）
     *
     * @param unitId 部门ID
     * @return 角色列表
     */
    List<RoleEntity> getUnitRoles(String unitId);

    /**
     * 加载角色的权限信息
     *
     * @param entity 角色对象
     */
    void loadPermissions(RoleEntity entity);

    /**
     * 加载角色的权限信息
     *
     * @param entities 角色对象列表
     */
    void loadPermissions(List<RoleEntity> entities);

    /**
     * 业务处理：保存一个角色信息
     *
     * @param entity 角色信息
     */
    void saveRole(RoleEntity entity);

    /**
     * 业务处理：修改一个角色信息
     *
     * @param entity 角色信息
     */
    void updateRole(RoleEntity entity);

    /**
     * 业务处理：修改角色信息的权限列表
     *
     * @param entity 角色信息
     */
    void setRolePermission(RoleEntity entity);

    /**
     * 业务处理：删除一个角色信息
     *
     * @param primaryKey 主键ID
     */
    void deleteRole(String primaryKey);

    /**
     * 业务处理：删除多个权限信息
     *
     * @param primaryKeys 主键ID列表
     */
    void deleteRole(List<String> primaryKeys);
}
