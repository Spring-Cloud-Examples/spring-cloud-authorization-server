package city.spring.service;

import city.spring.domain.entity.UnitEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 部门信息服务
 *
 * @author HouKunLin
 * @date 2019/12/8 0008 14:12
 */
public interface UnitService extends IService<UnitEntity> {
    String CACHE_NAME = "unit";
    /**
     * 获取用户的部门列表（包含权限）
     *
     * @param userId 用户ID
     * @return 部门列表
     */
    List<UnitEntity> getUserUnitsWithAuthority(String userId);

    /**
     * 获取用户的部门列表（不含权限）
     *
     * @param userId 用户ID
     * @return 部门列表
     */
    List<UnitEntity> getUserUnits(String userId);

    /**
     * 加载权限信息
     *
     * @param entity 部门
     */
    void loadPermissions(UnitEntity entity);

    /**
     * 加载权限信息
     *
     * @param entities 部门列表
     */
    void loadPermissions(List<UnitEntity> entities);

    /**
     * 加载角色信息
     *
     * @param entity 用户组
     */
    void loadRoles(UnitEntity entity);

    /**
     * 加载角色信息
     *
     * @param entities 用户组列表
     */
    void loadRoles(List<UnitEntity> entities);

    /**
     * 通过主键ID获取一个部门信息
     *
     * @param primaryKey      部门信息主键ID
     * @param loadPermissions 是否加载部门信息的特殊权限列表
     * @param loadRoles       是否加载部门信息的角色列表
     * @return 用户组信息 group info
     */
    UnitEntity getUnitInfo(String primaryKey, boolean loadPermissions, boolean loadRoles);

    /**
     * 业务处理：保存一个部门信息
     *
     * @param entity 部门信息
     */
    void saveUnit(UnitEntity entity);

    /**
     * 业务处理：修改一个部门信息
     *
     * @param entity 部门信息
     */
    void updateUnit(UnitEntity entity);

    /**
     * 业务处理：删除一个部门信息
     *
     * @param primaryKey 主键ID
     */
    void deleteUnit(String primaryKey);

    /**
     * 业务处理：删除多个部门信息
     *
     * @param primaryKeys 主键ID列表
     */
    void deleteUnit(List<String> primaryKeys);
}
