package city.spring.service;

import city.spring.domain.entity.UserAccountEntity;
import city.spring.domain.enums.IdentityType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 用户信息服务
 *
 * @author HouKunLin
 * @date 2019/12/22 0022 0:29
 */
public interface UserAccountService extends IService<UserAccountEntity> {
    /**
     * 获取用户的帐号信息列表
     *
     * @param userId 用户ID
     * @return 帐号列表
     */
    List<UserAccountEntity> getUserAccounts(String userId);

    /**
     * 获取用户的特定帐号类型的帐号列表
     *
     * @param userId 用户ID
     * @param type   帐号类型
     * @return 帐号列表
     */
    List<UserAccountEntity> getUserAccounts(String userId, IdentityType type);
}
