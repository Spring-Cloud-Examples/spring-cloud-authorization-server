package city.spring.service;

import city.spring.domain.entity.UserEntity;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * 用户信息服务
 *
 * @author HouKunLin
 * @date 2019/12/22 0022 0:29
 */
public interface UserService extends IService<UserEntity> {
    /**
     * 缓存存储名称
     */
    String CACHE_NAME = "user";

    /**
     * 分页获取用户信息
     *
     * @param pageable 分页参数
     * @return 用户列表
     */
    Page<UserEntity> getUsers(Pageable pageable);

    /**
     * 通过用户ID或者用户帐号名称获取用户信息
     *
     * @param idOrAccount 用户ID 或者 帐号登录名称
     * @return 用户信息
     */
    UserEntity getUserByIdOrAccount(String idOrAccount);

    /**
     * 通过用户ID或者用户帐号名称获取用户信息
     *
     * @param idOrAccount   用户ID 或者 帐号登录名称
     * @param loadAuthority 是否加载权限信息
     * @param userInfoEnums 需要加载的内容项
     * @return 用户信息
     */
    UserEntity getUserByIdOrAccount(String idOrAccount, boolean loadAuthority, UserInfoEnum... userInfoEnums);

    /**
     * 强制修改用户密码
     *
     * @param userId      用户ID
     * @param newPassword 新密码（原始密码，未加密状态）
     */
    void updatePassword(String userId, String newPassword);

    /**
     * 修改用户密码
     *
     * @param userId      用户ID
     * @param oldPassword 旧的密码
     * @param newPassword 新的密码
     */
    void updatePassword(String userId, String oldPassword, String newPassword);

    /**
     * 修改用户密码
     *
     * @param entity      用户信息
     * @param oldPassword 旧的密码
     * @param newPassword 新的密码
     */
    void updatePassword(UserEntity entity, String oldPassword, String newPassword);

    /**
     * 加载用户其他信息
     *
     * @param entity        用户
     * @param loadAuthority 是否加载权限信息
     * @param userInfoEnums 加载用户类型
     */
    void loadInfos(UserEntity entity, boolean loadAuthority, UserInfoEnum... userInfoEnums);

    /**
     * 加载一个用户信息和用户其他信息
     *
     * @param primaryKey    用户主键ID
     * @param loadAuthority 是否加载权限信息
     * @param userInfoEnums 加载用户类型
     * @return 用户信息
     */
    UserEntity getUserInfo(String primaryKey, boolean loadAuthority, UserInfoEnum... userInfoEnums);

    /**
     * 把用户信息同步到用户帐号信息（用户名、手机号、电子邮箱）
     *
     * @param entity 用户
     */
    void syncUserInfoToUserAccount(UserEntity entity);

    /**
     * 从用户帐号信息中同步信息到用户信息里面（用户名、手机号、电子邮箱）
     *
     * @param entity 用户
     */
    void syncUserAccountToUserInfo(UserEntity entity);

    /**
     * 业务处理：保存一个用户信息
     *
     * @param entity 用户信息
     */
    void saveUser(UserEntity entity);

    /**
     * 业务处理：修改一个用户信息
     *
     * @param entity 用户信息
     */
    void updateUser(UserEntity entity);

    /**
     * 业务处理：删除一个用户信息
     *
     * @param primaryKey 主键ID
     */
    void deleteUser(String primaryKey);

    /**
     * 业务处理：删除多个用户信息
     *
     * @param primaryKeys 主键ID列表
     */
    void deleteUser(List<String> primaryKeys);

    enum UserInfoEnum {
        /**
         * 所有信息
         */
        ALL,
        /**
         * 帐号信息
         */
        ACCOUNT,
        /**
         * 排除帐号信息，只获取用户组信息、用户部门信息、用户角色信息、用户权限信息
         */
        EX_ACCOUNT,
        /**
         * 用户组信息
         */
        GROUP,
        /**
         * 用户部门信息
         */
        UNIT,
        /**
         * 用户角色信息
         */
        ROLE,
        /**
         * 用户权限信息
         */
        PERMISSION,
    }
}
