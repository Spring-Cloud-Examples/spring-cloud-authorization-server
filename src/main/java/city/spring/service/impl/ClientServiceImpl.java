package city.spring.service.impl;

import city.spring.domain.entity.ClientEntity;
import city.spring.repository.ClientRepository;
import city.spring.service.ClientService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

/**
 * 客户端信息服务
 *
 * @author HouKunLin
 * @date 2020/3/22 0022 15:16
 */
@CacheConfig(cacheNames = {ClientServiceImpl.CACHE_NAME})
@Service
public class ClientServiceImpl extends ServiceImpl<ClientRepository, ClientEntity> implements ClientService {
}
