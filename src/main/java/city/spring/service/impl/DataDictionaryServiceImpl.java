package city.spring.service.impl;

import city.spring.domain.entity.DataDictionaryEntity;
import city.spring.domain.entity.DataDictionaryValueEntity;
import city.spring.repository.DataDictionaryRepository;
import city.spring.service.DataDictionaryService;
import city.spring.service.DataDictionaryValueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 数据字典类型 Service
 *
 * @author HouKunLin
 * @date 2020/3/26 0026 23:39
 */
@Transactional(rollbackFor = Throwable.class)
@Service
public class DataDictionaryServiceImpl extends ServiceImpl<DataDictionaryRepository, DataDictionaryEntity> implements DataDictionaryService {
    private final DataDictionaryValueService dataDictionaryValueService;

    public DataDictionaryServiceImpl(DataDictionaryValueService dataDictionaryValueService) {
        this.dataDictionaryValueService = dataDictionaryValueService;
    }

    @Override
    public void saveDataDictionaryType(DataDictionaryEntity entity) {
        boolean save = save(entity);
        if (!save) {
            throw new RuntimeException("保存数据字典类型失败");
        }
    }

    @Override
    public void updateDataDictionaryType(DataDictionaryEntity entity) {
        DataDictionaryEntity oldEntity = getById(entity.getId());
        boolean update = lambdaUpdate()
                .set(DataDictionaryEntity::getTitle, entity.getTitle())
                .set(DataDictionaryEntity::getType, entity.getType())
                .set(DataDictionaryEntity::getRemarks, entity.getRemarks())
                .set(DataDictionaryEntity::getEnabled, entity.getEnabled())
                .eq(DataDictionaryEntity::getId, entity.getId())
                .update();
        if (!update) {
            throw new RuntimeException("修改数据字典类型失败");
        }
        if (!StringUtils.equals(oldEntity.getType(), entity.getType())) {
            // 数据字典类型已经被修改，需要手动维护原来旧的类型值列表
            dataDictionaryValueService.lambdaUpdate()
                    .set(DataDictionaryValueEntity::getDictionaryId, entity.getType())
                    .eq(DataDictionaryValueEntity::getDictionaryId, oldEntity.getType())
                    .update();
        }
    }

    @Override
    public void deleteDataDictionaryType(String primaryKey) {
        removeById(primaryKey);
        dataDictionaryValueService.lambdaUpdate().eq(DataDictionaryValueEntity::getDictionaryId, primaryKey).remove();
    }

    @Override
    public void deleteDataDictionaryType(List<String> primaryKeys) {
        removeByIds(primaryKeys);
        dataDictionaryValueService.lambdaUpdate()
                .in(DataDictionaryValueEntity::getDictionaryId, primaryKeys)
                .remove();
    }

    @Override
    public void enabled(String primaryKey, boolean enabled) {
        lambdaUpdate()
                .set(DataDictionaryEntity::getEnabled, enabled)
                .eq(DataDictionaryEntity::getId, primaryKey)
                .update();
    }
}
