package city.spring.service.impl;

import city.spring.domain.entity.DataDictionaryValueEntity;
import city.spring.repository.DataDictionaryValueRepository;
import city.spring.service.DataDictionaryValueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 数据字典值信息 Service
 *
 * @author HouKunLin
 * @date 2020/3/26 0026 23:38
 */
@Service
public class DataDictionaryValueServiceImpl extends ServiceImpl<DataDictionaryValueRepository, DataDictionaryValueEntity> implements DataDictionaryValueService {
    @Override
    public void saveDataDictionaryValue(DataDictionaryValueEntity entity) {
        save(entity);
    }

    @Override
    public void updateDataDictionaryValue(DataDictionaryValueEntity entity) {
        lambdaUpdate()
                .set(DataDictionaryValueEntity::getDictionaryId, entity.getDictionaryId())
                .set(DataDictionaryValueEntity::getLabel, entity.getLabel())
                .set(DataDictionaryValueEntity::getValue, entity.getValue())
                .set(DataDictionaryValueEntity::getRemarks, entity.getRemarks())
                .set(DataDictionaryValueEntity::getEnabled, entity.getEnabled())
                .set(DataDictionaryValueEntity::getSorted, entity.getSorted())
                .eq(DataDictionaryValueEntity::getId, entity.getId())
                .update();
    }

    @Override
    public void deleteDataDictionaryValue(String primaryKey) {
        removeById(primaryKey);
    }

    @Override
    public void deleteDataDictionaryValue(List<String> primaryKeys) {
        removeByIds(primaryKeys);
    }

    @Override
    public void enabled(String primaryKey, boolean enabled) {
        lambdaUpdate()
                .set(DataDictionaryValueEntity::getEnabled, enabled)
                .eq(DataDictionaryValueEntity::getId, primaryKey)
                .update();
    }
}
