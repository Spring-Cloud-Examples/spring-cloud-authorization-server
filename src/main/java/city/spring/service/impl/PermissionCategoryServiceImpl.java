package city.spring.service.impl;

import city.spring.domain.entity.PermissionCategoryEntity;
import city.spring.repository.PermissionCategoryRepository;
import city.spring.service.PermissionCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

/**
 * 权限信息类别服务
 *
 * @author HouKunLin
 * @date 2020-03-26 09:33:08
 */
@CacheConfig(cacheNames = {PermissionCategoryServiceImpl.CACHE_NAME})
@Service
public class PermissionCategoryServiceImpl extends ServiceImpl<PermissionCategoryRepository, PermissionCategoryEntity> implements PermissionCategoryService {
}
