package city.spring.service.impl;

import city.spring.domain.entity.PermissionEntity;
import city.spring.modules.ext.entity.GroupPermissionExt;
import city.spring.modules.ext.entity.RolePermissionExt;
import city.spring.modules.ext.entity.UnitPermissionExt;
import city.spring.modules.ext.entity.UserPermissionExt;
import city.spring.modules.ext.service.GroupPermissionExtService;
import city.spring.modules.ext.service.RolePermissionExtService;
import city.spring.modules.ext.service.UnitPermissionExtService;
import city.spring.modules.ext.service.UserPermissionExtService;
import city.spring.repository.PermissionRepository;
import city.spring.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 权限信息服务
 *
 * @author HouKunLin
 * @date 2019/12/8 0008 14:54
 */
@CacheConfig(cacheNames = {PermissionServiceImpl.CACHE_NAME})
@Transactional(rollbackFor = Throwable.class)
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionRepository, PermissionEntity> implements PermissionService {
    private final GroupPermissionExtService groupPermissionExtService;
    private final RolePermissionExtService rolePermissionExtService;
    private final UnitPermissionExtService unitPermissionExtService;
    private final UserPermissionExtService userPermissionExtService;

    public PermissionServiceImpl(GroupPermissionExtService groupPermissionExtService, RolePermissionExtService rolePermissionExtService, UnitPermissionExtService unitPermissionExtService, UserPermissionExtService userPermissionExtService) {
        this.groupPermissionExtService = groupPermissionExtService;
        this.rolePermissionExtService = rolePermissionExtService;
        this.unitPermissionExtService = unitPermissionExtService;
        this.userPermissionExtService = userPermissionExtService;
    }

    @Cacheable(key = "'list:userId:'+#userId")
    @Override
    public List<PermissionEntity> getUserPermissions(String userId) {
        return baseMapper.getUserPermissions(userId);
    }

    @Cacheable(key = "'list:roleId:'+#roleId")
    @Override
    public List<PermissionEntity> getRolePermissions(String roleId) {
        return baseMapper.getRolePermissions(roleId);
    }

    @Cacheable(key = "'list:groupId:'+#groupId")
    @Override
    public List<PermissionEntity> getGroupPermissions(String groupId) {
        return baseMapper.getGroupPermissions(groupId);
    }

    @Cacheable(key = "'list:unitId:'+#unitId")
    @Override
    public List<PermissionEntity> getUnitPermissions(String unitId) {
        return baseMapper.getUnitPermissions(unitId);
    }

    @Override
    public void savePermission(PermissionEntity entity) {
        PermissionEntity findEntity = lambdaQuery().eq(PermissionEntity::getCode, entity.getCode()).one();
        if (findEntity != null) {
            throw new RuntimeException("已经存在该权限代码");
        }
        if (!save(entity)) {
            throw new RuntimeException("保存信息失败");
        }
    }

    @Override
    public void updatePermission(PermissionEntity entity) {
        PermissionEntity findEntity = lambdaQuery().eq(PermissionEntity::getCode, entity.getCode()).one();
        if (findEntity != null && !findEntity.getId().equals(entity.getId())) {
            throw new RuntimeException("已经存在该权限代码");
        }
        boolean update = lambdaUpdate()
                .set(PermissionEntity::getTitle, entity.getTitle())
                .set(PermissionEntity::getRemarks, entity.getRemarks())
                .set(PermissionEntity::getCode, entity.getCode())
                .eq(PermissionEntity::getId, entity.getId())
                .update();
        if (!update) {
            throw new RuntimeException("修改信息失败");
        }
    }

    @Override
    public void deletePermission(String primaryKey) {
        removeById(primaryKey);
        groupPermissionExtService.lambdaUpdate().eq(GroupPermissionExt::getRelatedId, primaryKey).remove();
        rolePermissionExtService.lambdaUpdate().eq(RolePermissionExt::getRelatedId, primaryKey).remove();
        unitPermissionExtService.lambdaUpdate().eq(UnitPermissionExt::getRelatedId, primaryKey).remove();
        userPermissionExtService.lambdaUpdate().eq(UserPermissionExt::getRelatedId, primaryKey).remove();
    }

    @Override
    public void deletePermission(List<String> primaryKeys) {
        removeByIds(primaryKeys);
        groupPermissionExtService.lambdaUpdate().in(GroupPermissionExt::getRelatedId, primaryKeys).remove();
        rolePermissionExtService.lambdaUpdate().in(RolePermissionExt::getRelatedId, primaryKeys).remove();
        unitPermissionExtService.lambdaUpdate().in(UnitPermissionExt::getRelatedId, primaryKeys).remove();
        userPermissionExtService.lambdaUpdate().in(UserPermissionExt::getRelatedId, primaryKeys).remove();
    }
}
