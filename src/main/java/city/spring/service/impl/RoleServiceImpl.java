package city.spring.service.impl;

import city.spring.domain.entity.PermissionEntity;
import city.spring.domain.entity.RoleEntity;
import city.spring.modules.ext.EntityExtUtils;
import city.spring.modules.ext.entity.GroupRoleExt;
import city.spring.modules.ext.entity.RolePermissionExt;
import city.spring.modules.ext.entity.UnitRoleExt;
import city.spring.modules.ext.entity.UserRoleExt;
import city.spring.modules.ext.service.GroupRoleExtService;
import city.spring.modules.ext.service.RolePermissionExtService;
import city.spring.modules.ext.service.UnitRoleExtService;
import city.spring.modules.ext.service.UserRoleExtService;
import city.spring.repository.RoleRepository;
import city.spring.service.PermissionService;
import city.spring.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 角色信息服务
 *
 * @author HouKunLin
 * @date 2019/12/8 0008 14:55
 */
@CacheConfig(cacheNames = {RoleServiceImpl.CACHE_NAME})
@Transactional(rollbackFor = Throwable.class)
@Service
public class RoleServiceImpl extends ServiceImpl<RoleRepository, RoleEntity> implements RoleService {
    private final PermissionService permissionService;
    private final RolePermissionExtService rolePermissionExtService;
    private final GroupRoleExtService groupRoleExtService;
    private final UnitRoleExtService unitRoleExtService;
    private final UserRoleExtService userRoleExtService;

    public RoleServiceImpl(PermissionService permissionService, RolePermissionExtService rolePermissionExtService, GroupRoleExtService groupRoleExtService, UnitRoleExtService unitRoleExtService, UserRoleExtService userRoleExtService) {
        this.permissionService = permissionService;
        this.rolePermissionExtService = rolePermissionExtService;
        this.groupRoleExtService = groupRoleExtService;
        this.unitRoleExtService = unitRoleExtService;
        this.userRoleExtService = userRoleExtService;
    }

    @Cacheable(key = "'list:authority:userId:'+#userId")
    @Override
    public List<RoleEntity> getUserRolesWithAuthority(String userId) {
        List<RoleEntity> roles = baseMapper.getUserRoles(userId);
        loadPermissions(roles);
        return roles;
    }

    @Cacheable(key = "'list:authority:groupId:'+#groupId")
    @Override
    public List<RoleEntity> getGroupRolesWithAuthority(String groupId) {
        List<RoleEntity> roles = baseMapper.getGroupRoles(groupId);
        loadPermissions(roles);
        return roles;
    }

    @Cacheable(key = "'list:authority:unitId:'+#unitId")
    @Override
    public List<RoleEntity> getUnitRolesWithAuthority(String unitId) {
        List<RoleEntity> roles = baseMapper.getUnitRoles(unitId);
        loadPermissions(roles);
        return roles;
    }

    @Cacheable(key = "'list:info:userId:'+#userId")
    @Override
    public List<RoleEntity> getUserRoles(String userId) {
        return baseMapper.getUserRoles(userId);
    }

    @Cacheable(key = "'list:info:groupId:'+#groupId")
    @Override
    public List<RoleEntity> getGroupRoles(String groupId) {
        return baseMapper.getGroupRoles(groupId);
    }

    @Cacheable(key = "'list:info:unitId:'+#unitId")
    @Override
    public List<RoleEntity> getUnitRoles(String unitId) {
        return baseMapper.getUnitRoles(unitId);
    }

    @Override
    public void loadPermissions(RoleEntity entity) {
        entity.setPermissions(permissionService.getRolePermissions(entity.getId()));
    }

    @Override
    public void loadPermissions(List<RoleEntity> entities) {
        entities.forEach(this::loadPermissions);
    }

    @Override
    public void saveRole(RoleEntity entity) {
        RoleEntity findEntity = lambdaQuery().eq(RoleEntity::getCode, entity.getCode()).one();
        if (findEntity != null) {
            throw new RuntimeException("已经存在该角色代码");
        }
        save(entity);
        setRolePermission(entity);
    }

    @Override
    public void updateRole(RoleEntity entity) {
        RoleEntity findEntity = lambdaQuery().eq(RoleEntity::getCode, entity.getCode()).one();
        if (findEntity != null && !findEntity.getId().equals(entity.getId())) {
            throw new RuntimeException("已经存在该角色代码");
        }
        lambdaUpdate()
                .set(RoleEntity::getTitle, entity.getTitle())
                .set(RoleEntity::getRemarks, entity.getRemarks())
                .set(RoleEntity::getCode, entity.getCode())
                .eq(RoleEntity::getId, entity.getId()).update();
        setRolePermission(entity);
    }

    @Override
    public void setRolePermission(RoleEntity entity) {
        // 删除旧的 角色-权限 关联关系  后 再  添加新的关联关系
        EntityExtUtils.repairRelation(rolePermissionExtService, entity,
                RoleEntity::getId, RoleEntity::getPermissions, PermissionEntity::getId,
                RolePermissionExt::new, RolePermissionExt::getRoleId);
    }

    @Override
    public void deleteRole(String primaryKey) {
        removeById(primaryKey);
        rolePermissionExtService.lambdaUpdate().eq(RolePermissionExt::getRoleId, primaryKey).remove();
        groupRoleExtService.lambdaUpdate().eq(GroupRoleExt::getRelatedId, primaryKey).remove();
        unitRoleExtService.lambdaUpdate().eq(UnitRoleExt::getRelatedId, primaryKey).remove();
        userRoleExtService.lambdaUpdate().eq(UserRoleExt::getRelatedId, primaryKey).remove();
    }

    @Override
    public void deleteRole(List<String> primaryKeys) {
        removeByIds(primaryKeys);
        rolePermissionExtService.lambdaUpdate().in(RolePermissionExt::getRoleId, primaryKeys).remove();
        groupRoleExtService.lambdaUpdate().in(GroupRoleExt::getRelatedId, primaryKeys).remove();
        unitRoleExtService.lambdaUpdate().in(UnitRoleExt::getRelatedId, primaryKeys).remove();
        userRoleExtService.lambdaUpdate().in(UserRoleExt::getRelatedId, primaryKeys).remove();
    }
}
