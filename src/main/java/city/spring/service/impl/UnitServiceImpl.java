package city.spring.service.impl;

import city.spring.domain.entity.PermissionEntity;
import city.spring.domain.entity.RoleEntity;
import city.spring.domain.entity.UnitEntity;
import city.spring.modules.ext.EntityExtUtils;
import city.spring.modules.ext.entity.UnitPermissionExt;
import city.spring.modules.ext.entity.UnitRoleExt;
import city.spring.modules.ext.entity.UserUnitExt;
import city.spring.modules.ext.service.UnitPermissionExtService;
import city.spring.modules.ext.service.UnitRoleExtService;
import city.spring.modules.ext.service.UserUnitExtService;
import city.spring.repository.UnitRepository;
import city.spring.service.PermissionService;
import city.spring.service.RoleService;
import city.spring.service.UnitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 部门信息服务
 *
 * @author HouKunLin
 * @date 2019/12/8 0008 14:57
 */
@CacheConfig(cacheNames = {UnitServiceImpl.CACHE_NAME})
@Service
public class UnitServiceImpl extends ServiceImpl<UnitRepository, UnitEntity> implements UnitService {
    private final RoleService roleService;
    private final PermissionService permissionService;
    private final UnitRoleExtService unitRoleExtService;
    private final UnitPermissionExtService unitPermissionExtService;
    private final UserUnitExtService userUnitExtService;

    public UnitServiceImpl(RoleService roleService, PermissionService permissionService, UnitRoleExtService unitRoleExtService, UnitPermissionExtService unitPermissionExtService, UserUnitExtService userUnitExtService) {
        this.roleService = roleService;
        this.permissionService = permissionService;
        this.unitRoleExtService = unitRoleExtService;
        this.unitPermissionExtService = unitPermissionExtService;
        this.userUnitExtService = userUnitExtService;
    }

    @Cacheable(key = "'list:authority:userId:'+#userId")
    @Override
    public List<UnitEntity> getUserUnitsWithAuthority(String userId) {
        List<UnitEntity> units = baseMapper.getUserUnits(userId);
        for (UnitEntity unit : units) {
            loadRoles(unit);
            loadPermissions(unit);
        }
        return units;
    }

    @Cacheable(key = "'list:info:userId:'+#userId")
    @Override
    public List<UnitEntity> getUserUnits(String userId) {
        return baseMapper.getUserUnits(userId);
    }

    @Override
    public void loadPermissions(UnitEntity entity) {
        entity.setPermissions(permissionService.getUnitPermissions(entity.getId()));
    }

    @Override
    public void loadPermissions(List<UnitEntity> entities) {
        entities.forEach(this::loadPermissions);
    }

    @Override
    public void loadRoles(UnitEntity entity) {
        entity.setRoles(roleService.getUnitRolesWithAuthority(entity.getId()));
    }

    @Override
    public void loadRoles(List<UnitEntity> entities) {
        entities.forEach(this::loadRoles);
    }

    @Override
    public UnitEntity getUnitInfo(String primaryKey, boolean loadPermissions, boolean loadRoles) {
        UnitEntity entity = getById(primaryKey);
        if (loadPermissions) {
            loadPermissions(entity);
        }
        if (loadRoles) {
            loadRoles(entity);
        }
        return entity;
    }

    @Override
    public void saveUnit(UnitEntity entity) {
        boolean save = save(entity);
        if (!save) {
            throw new RuntimeException("保存部门信息失败");
        }
        repairRelation(entity);
    }

    @Override
    public void updateUnit(UnitEntity entity) {
        boolean update = lambdaUpdate()
                .set(UnitEntity::getTitle, entity.getTitle())
                .set(UnitEntity::getRemarks, entity.getRemarks())
                .eq(UnitEntity::getId, entity.getId())
                .update();
        if (!update) {
            throw new RuntimeException("修改部门信息失败");
        }
        repairRelation(entity);
    }

    @Override
    public void deleteUnit(String primaryKey) {
        removeById(primaryKey);
        unitRoleExtService.lambdaUpdate().eq(UnitRoleExt::getUnitId, primaryKey).remove();
        unitPermissionExtService.lambdaUpdate().eq(UnitPermissionExt::getUnitId, primaryKey).remove();
        userUnitExtService.lambdaUpdate().eq(UserUnitExt::getRelatedId, primaryKey).remove();
    }

    @Override
    public void deleteUnit(List<String> primaryKeys) {
        removeByIds(primaryKeys);
        unitRoleExtService.lambdaUpdate().in(UnitRoleExt::getUnitId, primaryKeys).remove();
        unitPermissionExtService.lambdaUpdate().in(UnitPermissionExt::getUnitId, primaryKeys).remove();
        userUnitExtService.lambdaUpdate().in(UserUnitExt::getRelatedId, primaryKeys).remove();
    }

    /**
     * 维护 <strong>部门信息</strong> 的 角色列表、权限列表
     *
     * @param entity <strong>部门信息</strong>
     */
    private void repairRelation(UnitEntity entity) {
        EntityExtUtils.repairRelation(unitRoleExtService, entity,
                UnitEntity::getId, UnitEntity::getRoles, RoleEntity::getId,
                UnitRoleExt::new, UnitRoleExt::getUnitId);

        EntityExtUtils.repairRelation(unitPermissionExtService, entity,
                UnitEntity::getId, UnitEntity::getPermissions, PermissionEntity::getId,
                UnitPermissionExt::new, UnitPermissionExt::getUnitId);
    }
}
