package city.spring.service.impl;

import city.spring.domain.entity.UserAccountEntity;
import city.spring.domain.enums.IdentityType;
import city.spring.repository.UserAccountRepository;
import city.spring.service.UserAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户帐号信息服务
 *
 * @author HouKunLin
 * @date 2019/12/22 0022 16:14
 */
@Service
public class UserAccountServiceImpl extends ServiceImpl<UserAccountRepository, UserAccountEntity> implements UserAccountService {
    @Override
    public List<UserAccountEntity> getUserAccounts(String userId) {
        return lambdaQuery().eq(UserAccountEntity::getUserId, userId).list();
    }

    @Override
    public List<UserAccountEntity> getUserAccounts(String userId, IdentityType type) {
        return lambdaQuery().eq(UserAccountEntity::getUserId, userId)
                .eq(UserAccountEntity::getIdentityType, type)
                .list();
    }
}
