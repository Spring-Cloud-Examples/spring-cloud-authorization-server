package city.spring.service.impl;

import city.spring.domain.entity.*;
import city.spring.domain.enums.IdentityType;
import city.spring.modules.ext.EntityExtUtils;
import city.spring.modules.ext.entity.UserGroupExt;
import city.spring.modules.ext.entity.UserPermissionExt;
import city.spring.modules.ext.entity.UserRoleExt;
import city.spring.modules.ext.entity.UserUnitExt;
import city.spring.modules.ext.service.UserGroupExtService;
import city.spring.modules.ext.service.UserPermissionExtService;
import city.spring.modules.ext.service.UserRoleExtService;
import city.spring.modules.ext.service.UserUnitExtService;
import city.spring.mq.SmsNoticeTopic;
import city.spring.repository.UserRepository;
import city.spring.service.*;
import city.spring.utils.MyBatisUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.domain.Pageable;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.function.Consumer3;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * 用户信息服务
 *
 * @author HouKunLin
 * @date 2019/12/22 0022 0:30
 */
@CacheConfig(cacheNames = {UserServiceImpl.CACHE_NAME})
@Transactional(rollbackFor = Throwable.class)
@Service
public class UserServiceImpl extends ServiceImpl<UserRepository, UserEntity> implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final boolean isDebug = logger.isDebugEnabled();
    private final GroupService groupService;
    private final UnitService unitService;
    private final RoleService roleService;
    private final PermissionService permissionService;
    private final UserAccountService userAccountService;
    private final PasswordEncoder passwordEncoder;
    private final SmsNoticeTopic smsNoticeTopic;
    private final UserRoleExtService userRoleExtService;
    private final UserPermissionExtService userPermissionExtService;
    private final UserGroupExtService userGroupExtService;
    private final UserUnitExtService userUnitExtService;

    public UserServiceImpl(GroupService groupService, UnitService unitService, RoleService roleService, PermissionService permissionService, UserAccountService userAccountService, PasswordEncoder passwordEncoder, SmsNoticeTopic smsNoticeTopic, UserRoleExtService userRoleExtService, UserPermissionExtService userPermissionExtService, UserGroupExtService userGroupExtService, UserUnitExtService userUnitExtService) {
        this.groupService = groupService;
        this.unitService = unitService;
        this.roleService = roleService;
        this.permissionService = permissionService;
        this.userAccountService = userAccountService;
        this.passwordEncoder = passwordEncoder;
        this.smsNoticeTopic = smsNoticeTopic;
        this.userRoleExtService = userRoleExtService;
        this.userPermissionExtService = userPermissionExtService;
        this.userGroupExtService = userGroupExtService;
        this.userUnitExtService = userUnitExtService;
    }

    @Override
    public Page<UserEntity> getUsers(Pageable pageable) {
        Page<UserEntity> userEntityPage = MyBatisUtils.toPage(pageable);
        QueryWrapper<UserEntity> userEntityQueryWrapper = new QueryWrapper<>();
        return baseMapper.selectPage(userEntityPage, userEntityQueryWrapper);
    }

    @Override
    public UserEntity getUserByIdOrAccount(String idOrAccount) {
        return baseMapper.findByUserIdOrAccountIdentifier(idOrAccount);
    }

    @Override
    public UserEntity getUserByIdOrAccount(String idOrAccount, boolean loadAuthority, UserInfoEnum... userInfoEnums) {
        UserEntity userEntity = getUserByIdOrAccount(idOrAccount);
        if (userEntity == null) {
            if (isDebug) {
                QueryWrapper<UserAccountEntity> accountEntityQueryWrapper = new QueryWrapper<>();
                accountEntityQueryWrapper.eq("identifier", idOrAccount);
                UserAccountEntity userAccountEntity = userAccountService.getOne(accountEntityQueryWrapper);
                if (userAccountEntity == null) {
                    logger.debug("找不到该用户帐号: {}", idOrAccount);
                    throw new RuntimeException("找不到该帐号");
                }
                logger.debug("找到用户帐号，但是找不到用户，这是一个悬空的用户帐号信息：{}", userAccountEntity);
                throw new RuntimeException("这是一个未绑定用户信息的帐号");
            }
            throw new RuntimeException("找不到该用户");
        }
        this.loadInfos(userEntity, loadAuthority, userInfoEnums);
        return userEntity;
    }

    @Override
    public void updatePassword(String userId, String newPassword) {
        baseMapper.updatePassword(userId, passwordEncoder.encode(newPassword));
    }

    @Override
    public void updatePassword(String userId, String oldPassword, String newPassword) {
        updatePassword(getById(userId), oldPassword, newPassword);
    }

    @Override
    public void updatePassword(UserEntity entity, String oldPassword, String newPassword) {
        if (!passwordEncoder.matches(oldPassword, entity.getPassword())) {
            throw new RuntimeException("密码验证错误");
        }
        baseMapper.updatePassword(entity.getId(), passwordEncoder.encode(newPassword));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("user", entity);
        map.put("newPassword", newPassword);
        smsNoticeTopic.output().send(MessageBuilder.withPayload(map).build());
    }

    @Override
    public void loadInfos(UserEntity entity, boolean loadAuthority, UserInfoEnum... userInfoEnums) {
        if (entity == null || userInfoEnums.length == 0) {
            return;
        }
        boolean isAllInfo = ArrayUtils.contains(userInfoEnums, UserInfoEnum.ALL);
        boolean isExAccount = ArrayUtils.contains(userInfoEnums, UserInfoEnum.EX_ACCOUNT);

        String entityId = entity.getId();
        Function<String, List<GroupEntity>> groupFunc;
        Function<String, List<UnitEntity>> unitFunc;
        Function<String, List<RoleEntity>> roleFunc;
        if (loadAuthority) {
            groupFunc = groupService::getUserGroupsWithAuthority;
            unitFunc = unitService::getUserUnitsWithAuthority;
            roleFunc = roleService::getUserRolesWithAuthority;
        } else {
            groupFunc = groupService::getUserGroups;
            unitFunc = unitService::getUserUnits;
            roleFunc = roleService::getUserRoles;
        }
        if (isAllInfo || isExAccount || ArrayUtils.contains(userInfoEnums, UserInfoEnum.GROUP)) {
            entity.setGroups(groupFunc.apply(entityId));
        }
        if (isAllInfo || isExAccount || ArrayUtils.contains(userInfoEnums, UserInfoEnum.UNIT)) {
            entity.setUnits(unitFunc.apply(entityId));
        }
        if (isAllInfo || isExAccount || ArrayUtils.contains(userInfoEnums, UserInfoEnum.ROLE)) {
            entity.setRoles(roleFunc.apply(entityId));
        }
        if (isAllInfo || isExAccount || ArrayUtils.contains(userInfoEnums, UserInfoEnum.PERMISSION)) {
            entity.setPermissions(permissionService.getUserPermissions(entityId));
        }
        if (isAllInfo || ArrayUtils.contains(userInfoEnums, UserInfoEnum.ACCOUNT)) {
            entity.setAccounts(userAccountService.getUserAccounts(entityId));
        }
    }

    @Override
    public UserEntity getUserInfo(String primaryKey, boolean loadAuthority, UserInfoEnum... userInfoEnums) {
        UserEntity entity = getById(primaryKey);
        loadInfos(entity, loadAuthority, userInfoEnums);
        return entity;
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void syncUserInfoToUserAccount(UserEntity entity) {
        syncUserAccountAction(entity, (entityFieldFunction, account, identityType) -> {
            String value = entityFieldFunction.apply(entity);
            if (account == null) {
                // 用户信息里面存在：用户名、手机号、电子邮箱，但是在用户帐号里面没有这个帐号，因此要添加一个账号
                if (StringUtils.isBlank(value)) {
                    return;
                }
                // 自动创建这个用户帐号信息
                UserAccountEntity userAccountEntity = new UserAccountEntity();
                userAccountEntity.setUserId(entity.getId());
                userAccountEntity.setIdentifier(value);
                userAccountEntity.setVerified(true);
                userAccountEntity.setIdentityType(identityType);
                userAccountService.save(userAccountEntity);
                return;
            }
            if (!account.getIdentifier().equals(value)) {
                userAccountService.lambdaUpdate()
                        .set(UserAccountEntity::getIdentifier, value)
                        .eq(UserAccountEntity::getId, account.getId())
                        .update();
            }
        });
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void syncUserAccountToUserInfo(UserEntity entity) {
        syncUserAccountAction(entity, (entityFieldFunction, account, identityType) -> {
            String value = entityFieldFunction.apply(entity);
            if (account != null && !account.getIdentifier().equals(value)) {
                this.lambdaUpdate()
                        .set(entityFieldFunction, account.getIdentifier())
                        .eq(UserEntity::getId, entity.getId())
                        .update();
            }
        });
    }

    /**
     * 判断一个待操作用户的用户名、电子邮件、手机号信息是否存在系统中，如果存在系统中，并且不是待操作用户，则抛出异常。
     * 因为系统允许用户使用用户名、电子邮件、手机号来登录系统，因此这些数据在系统中必须唯一
     *
     * @param entity 需要操作的用户信息
     */
    private void checkExistsUserAccount(UserEntity entity) {
        BiConsumer<UserEntity, String> callback = (findEntity, message) -> {
            if (findEntity != null && !findEntity.getId().equals(entity.getId())) {
                // 找到的用户信息不为null，并且这个找到的用户ID不是当前操作的用户ID，则判断已经存在该帐号的用户
                throw new RuntimeException(message);
            }
        };
        // 是否存在用户名
        callback.accept(baseMapper.findByUsernameAccount(entity.getUsername()), "用户名已存在");
        // 是否存在电子邮件
        callback.accept(baseMapper.findByEmailAccount(entity.getEmail()), "电子邮件已存在");
        // 是否存在手机号
        callback.accept(baseMapper.findByPhoneAccount(entity.getPhone()), "手机号已存在");
    }

    /**
     * 业务处理：保存一个用户信息
     *
     * @param entity 用户信息
     */
    @Override
    public void saveUser(UserEntity entity) {
        checkExistsUserAccount(entity);
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        boolean save = save(entity);
        if (!save) {
            throw new RuntimeException("保存用户信息失败");
        }
        syncUserInfoToUserAccount(entity);
        repairRelation(entity);
    }

    @Override
    public void updateUser(UserEntity entity) {
        checkExistsUserAccount(entity);
        boolean update = lambdaUpdate()
                .eq(UserEntity::getId, entity.getId())
                .set(UserEntity::getNickname, entity.getNickname())
                .set(UserEntity::getEmail, entity.getEmail())
                .set(UserEntity::getPhone, entity.getPhone())
                .set(UserEntity::getIsAccountNonExpired, entity.getIsAccountNonExpired())
                .set(UserEntity::getIsAccountNonLocked, entity.getIsAccountNonLocked())
                .set(UserEntity::getIsCredentialsNonExpired, entity.getIsCredentialsNonExpired())
                .set(UserEntity::getIsEnabled, entity.getIsEnabled())
                .update();
        if (!update) {
            throw new RuntimeException("修改用户信息失败");
        }
        syncUserInfoToUserAccount(entity);
        repairRelation(entity);
    }

    @Override
    public void deleteUser(String primaryKey) {
        if (count() > 1) {
            removeById(primaryKey);
            userAccountService.lambdaUpdate().eq(UserAccountEntity::getUserId, primaryKey).remove();
            userRoleExtService.lambdaUpdate().eq(UserRoleExt::getUserId, primaryKey).remove();
            userPermissionExtService.lambdaUpdate().eq(UserPermissionExt::getUserId, primaryKey).remove();
            userGroupExtService.lambdaUpdate().eq(UserGroupExt::getUserId, primaryKey).remove();
            userUnitExtService.lambdaUpdate().eq(UserUnitExt::getUserId, primaryKey).remove();
        }
    }

    @Override
    public void deleteUser(List<String> primaryKeys) {
        if (count() > 1) {
            removeByIds(primaryKeys);
            userAccountService.lambdaUpdate().in(UserAccountEntity::getUserId, primaryKeys).remove();
            userRoleExtService.lambdaUpdate().in(UserRoleExt::getUserId, primaryKeys).remove();
            userPermissionExtService.lambdaUpdate().in(UserPermissionExt::getUserId, primaryKeys).remove();
            userGroupExtService.lambdaUpdate().in(UserGroupExt::getUserId, primaryKeys).remove();
            userUnitExtService.lambdaUpdate().in(UserUnitExt::getUserId, primaryKeys).remove();
        }
    }

    /**
     * 抽取通用方法，处理指定帐号类型的信息
     *
     * @param entity     用户
     * @param biConsumer 信息比对处理方法。
     *                   <p>参数一SFunction&lt;UserEntity, String&gt;：用户对象对应的字段get方法</p>
     *                   <p>参数二UserAccountEntity：该用户字段对应的帐号信息，帐号信息可能为null</p>
     *                   <p>参数三IdentityType：这个帐号信息的帐号类型，如果帐号信息为null的时候，可以通过账号类型来创建一个账号信息</p>
     */
    private void syncUserAccountAction(UserEntity entity, Consumer3<SFunction<UserEntity, String>, UserAccountEntity, IdentityType> biConsumer) {
        // 获取用户的帐号列表
        List<UserAccountEntity> userAccounts = userAccountService.getUserAccounts(entity.getId());
        // 从账户列表中获取指定类型的帐号信息
        Function<IdentityType, UserAccountEntity> getUserAccount = (type) -> {
            for (UserAccountEntity userAccount : userAccounts) {
                if (type.equals(userAccount.getIdentityType())) {
                    return userAccount;
                }
            }
            return null;
        };
        // 需要同步的帐号类型，该类型应该要保持与用户信息的字段一致
        IdentityType[] identityTypes = new IdentityType[]{IdentityType.USERNAME, IdentityType.PHONE, IdentityType.EMAIL};
        // 帐号类型所对应的用户字段方法（一一对应）
        List<SFunction<UserEntity, String>> fieldFunctions = Arrays.asList(UserEntity::getUsername, UserEntity::getPhone, UserEntity::getEmail);

        for (int i = 0; i < identityTypes.length; i++) {
            IdentityType identityType = identityTypes[i];
            // 帐号类型对应的获取数据方法
            SFunction<UserEntity, String> fieldFunction = fieldFunctions.get(i);
            // 获取该帐号类型的帐号信息
            UserAccountEntity accountEntity = getUserAccount.apply(identityType);
            biConsumer.accept(fieldFunction, accountEntity, identityType);
        }
    }

    /**
     * 维护 <strong>用户信息</strong> 的 用户组列表、部门列表、角色列表、权限列表
     *
     * @param entity 用户信息
     */
    private void repairRelation(UserEntity entity) {
        EntityExtUtils.repairRelation(userGroupExtService, entity,
                UserEntity::getId, UserEntity::getGroups, GroupEntity::getId,
                UserGroupExt::new, UserGroupExt::getUserId);

        EntityExtUtils.repairRelation(userUnitExtService, entity,
                UserEntity::getId, UserEntity::getUnits, UnitEntity::getId,
                UserUnitExt::new, UserUnitExt::getUserId);

        EntityExtUtils.repairRelation(userRoleExtService, entity,
                UserEntity::getId, UserEntity::getRoles, RoleEntity::getId,
                UserRoleExt::new, UserRoleExt::getUserId);

        EntityExtUtils.repairRelation(userPermissionExtService, entity,
                UserEntity::getId, UserEntity::getPermissions, PermissionEntity::getId,
                UserPermissionExt::new, UserPermissionExt::getUserId);
    }
}
